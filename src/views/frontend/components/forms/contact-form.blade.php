<form method="post" action="{{ localeUrl('/contact') }}" class="recaptcha">
    <h3 class="text-lg lg:text-2xl leading-normal text-center tracking-tight font-bold text-primary mb-7 header-text">{!! dynamicContent($pageContents, 'form-title') !!}</h3>

    <input name="name" type="text" placeholder="{{ trans('contact.full_name') }}" class="rounded-full bg-whiter h-14 w-full px-4 mb-3">
    <input name="email" type="email" placeholder="{{ trans('contact.email_address') }}" class="rounded-full bg-whiter h-14 w-full px-4 mb-3">
    <input name="tel" type="text" placeholder="{{ trans('contact.telephone_number') }}" class="rounded-full bg-whiter h-14 w-full px-4 mb-3">
    <textarea name="message" id="comments" cols="30" rows="10" placeholder="{{ trans('contact.your_message') }}" class="rounded-3xl bg-whiter h-32 w-full px-4 py-3 mb-3"></textarea>

    @include(themeViewPath('frontend.forms.recaptcha.recaptcha-fallback-inputs'), ['inputClass' => 'rounded-full bg-whiter h-14 w-full px-4 mb-3'])

    <div class="text-center">
        <button type="submit" class="text-base text-center tracking-wide font-bold text-white cta px-4 sm:px-9 rounded-full h-9 sm:h-12 right-1 top-1 hover:bg-secondary transition-all">{{ trans('button.send_enquiry') }}</button>
    </div>
    @csrf
</form>
