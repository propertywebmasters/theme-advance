@if (hasFeature(\App\Models\TenantFeature::FEATURE_SOCIAL_FIXED_TAB))

    @php
        $service = app()->make(\App\Services\SocialAccount\Contracts\SocialAccountServiceInterface::class);
        $socialAccounts = $service->get();

        $socialCount = 0;
        if ($socialAccounts !== null) {
            $networks = ['twitter', 'linked_in', 'facebook', 'instagram', 'tiktok'];
            foreach ($networks as $network) {
                if ($socialAccounts->$network !== null) {
                    $socialCount++;
                }
            }
        }
    @endphp

    @if($socialCount > 0)
        <div id="fixed-social-tab" class="hidden lg:block fixed bottom-16 z-30" style="right: 1.25rem;">
            <div class="grid grid-cols-1 gap-2">
                @foreach($networks as $network)
                    @if($socialAccounts->$network === null)
                        @continue
                    @endif
                    <div class="text-center flex items-center justify-center cta text-white shadow social-icon-container shadow" style="width: 40px; height: 40px; border-radius: 30px;">
                        <a href="{{ $socialAccounts->$network }}" class="block p-2" target="_BLANK">
                            <img src="{{ themeImage($network.'.svg') }}" class="svg-inject text-white fill-current" alt="{{ $network }}">
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    @endif
@endif

