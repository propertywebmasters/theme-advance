@if($testimonials->count() > 0)

    @php
    $testimonialCount = $testimonials->count() >= 3 ? 3 : 1
    @endphp

    <section id="testimonials" class="bg-whiter py-12 md:py-16 py-12" data-slides-per-view="{{ $testimonialCount }}">
        <div class="container mx-auto px-4">
            <h3 class="header-text py-2 text-2xl md:text-3xl text-center tracking-tight text-primary">{{ trans('header.what_our_customers_think') }}</h3>
        </div>
        <!-- Swiper -->
        <div class="container mx-auto md:px-4 px-2 overflow-hidden">
            <div class="testimonial-swiper">
                <!-- Additional required wrapper -->
                <div class="swiper-wrapper">
                    <!-- Slides -->
                    @foreach ($testimonials as $testimonial)
                        @include(themeViewPath('frontend.components.cards.testimonial'))
                    @endforeach
                </div>
                <!-- If we need pagination -->
                <div class="swiper-pagination"></div>

                <!-- If we need scrollbar -->
                <div class="swiper-scrollbar"></div>
            </div>

            <div class="text-center pt-4">
                <a class="rounded-3xl inline-block primary-bg text-white md:text-lg py-2 px-4 hover-lighten" href="{{ localeUrl('/testimonials') }}">
                    {{  trans('header.view_archive') }} <img class="svg-inject inline-block text-white stroke-current fill-current h-4 pl-2" src="{{ themeImage('icons/right-arrow.svg') }}" loading="lazy">
                </a>
            </div>

        </div>
    </section>
@endif