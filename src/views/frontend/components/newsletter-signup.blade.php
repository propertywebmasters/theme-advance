<section class="relative z-10 footer-bg pt-4">
    <div class="container mx-auto border-b pb-6">
    <form id="newsletter-signup" method="post" action="javascript:" enctype="application/x-www-form-urlencoded">
        <div class="container mx-auto px-4 md:px-0 xl:px-4 my-2">
            <div class="grid grid-cols-1 md:grid-cols-5 space-x-0">

                <div class="col-span-2 text-center pb-4 md:pb-0">
                    <h3 class="header-text text-xl md:text-3xl leading-tight mb-1">{{ trans('mailing-list.signup_to_our_newsletter') }}</h3>
                    <span class="text-base leading-tight tracking-tight">{!! trans('mailing-list.stay_upto_date_with_our_latest_news') !!}</span>
                </div>

                <div>
                    <input name="newsletter_name" type="text" class="w-full rounded-full md:rounded-r-none border-gray-50 border-2 px-4 sm:h-14 h-11 focus:border-0 focus:outline-none text-black" placeholder="{{ trans('contact.full_name') }}" required>
                </div>


                <div class="col-span-2 relative mt-5 md:mt-0">
                    <input name="newsletter_email" type="email" class="w-full rounded-full md:rounded-l-none border-gray-50 border-2 px-4 sm:h-14 h-11 focus:border-0 focus:outline-none text-black" placeholder="{{ trans('placeholder.enter_email_address') }}" required>
                    <button type="submit" class="text-base text-center tracking-wide font-bold text-white cta cta-text px-4 sm:px-9 rounded-full h-9 sm:h-12  right-1 top-1 transition-all absolute">{{ trans('mailing-list.signup') }}</button>
                </div>

            </div>
        </div>
        @csrf
    </form>
    </div>
</section>
