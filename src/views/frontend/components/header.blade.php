@php
    use App\Http\Controllers\Frontend\AboutController;
    use App\Http\Controllers\Frontend\ContactController;
    use App\Http\Controllers\Frontend\HomeController;
    use App\Http\Controllers\Frontend\ValuationController;
    use App\Models\Currency;use App\Models\SiteSetting;
    use App\Models\TenantFeature;use App\Services\SiteSetting\Contracts\SiteSettingServiceInterface;use Illuminate\Support\Str;
    $email = app()->make(SiteSettingServiceInterface::class)->retrieve(SiteSetting::SETTING_SITE_EMAIL['key']);
    $siteTel = app()->make(SiteSettingServiceInterface::class)->retrieve(SiteSetting::SETTING_SITE_TEL['key']);

    $logoPath = assetPath(app()->make(SiteSettingServiceInterface::class)->retrieve(SiteSetting::SETTING_LOGO['key'], config('platform-cache.site_settings_enabled')));
    $controller = get_class(request()->route()->getController());
    $method = request()->route()->getActionMethod();

    // only add transparency flag on homepage
    $headerClass = $controller === HomeController::class && $method === 'index'
        ? 'bg-transparent absolute'
        : 'navigation-bg';

    // $headerClass = $controller === ValuationController::class && $method === 'index'
    //     ? 'valuation'
    //     : $headerClass;

    // $headerClass = $controller === ContactController::class && $method === 'index'
    //         ? 'contact'
    //         : $headerClass;

    // $headerClass = $controller === AboutController::class && $method === 'index'
    //         ? 'about'
    //         : $headerClass;

    $currentCurrency = currentCurrency()
@endphp

@if(env('CSS_BREAKPOINT_HELPER'))
    @include('frontend.components.helpers.breakpoint-helper')
@endif

@if (hasFeature(TenantFeature::FEATURE_FIXED_GET_VALUATION_BUTTON))
    @include(themeViewPath('frontend.components.fixed-get-valuation-button'))
@else
    @include(themeViewPath('frontend.components.fixed-social'))
@endif

{{-- TODO: remove feature flag? --}}
{{-- @if(hasFeature(\App\Models\TenantFeature::FEATURE_CONTACT_HEADER))
    @include(themeViewPath('frontend.components.contact-header'))
@endif --}}

<nav id="main-nav" class="navigation-bg w-full z-20 {{ $headerClass }}">
    <div class="mx-auto flex justify-between flex-col lg:flex-row relative lg:items-center lg:px-4">
        <div class="relative p-4">
            <a id="site-logo" href="{{ localeUrl('/') }}">
                <img class="logo relative -top-1 h-10 lg:h-12 z-10" src="{{ $logoPath }}" alt="logo" loading="lazy">
            </a>
        </div>
        <div class="hidden lg:block mobile-menu absolute lg:relative left-0 top-12 lg:top-0 lg:pb-0 pb-5 w-full lg:w-auto z-40 navigation-bg lg:bg-transparent">
            <ul class="flex flex-col lg:flex-row nav-text items-center">
                @foreach ($navigation as $nav)
                    @php
                        $anchorLabel = getLocaleAnchorLabel($nav)
                    @endphp

                    <li class="top-level relative">
                        <a class="text-base text-right tracking-tight px-3 py-3 inline-block transition-all @if($nav->class !== null) {{ $nav->class }} @endif"
                           href="{{ $nav->children->count() ? 'javascript:;' : localeUrl($nav->url) }}" data-target="menu-{{ Str::slug($anchorLabel) }}">
                            {{ $anchorLabel }}
                            @if ($nav->children->count() > 0)
                                <img class="down-carat svg-inject inline-block h-2 ml-2" src="{{ themeImage('down-carat.svg') }}" alt="&or;">
                            @endif
                        </a>
                        @if($nav->children->count() > 0)
                            <div class="relative lg:absolute lg:left-0 hidden lg:-translate-x-1/2 lg:top-10 menu-{{ Str::slug($anchorLabel) }}">
                                <ul class="primary-bg text-gray-500 w-auto text-sm xl:text-md text-gray-900 lg:shadow no-drag dropdown-menu">
                                    @foreach ($nav->children as $child)
                                        @php
                                            $childAnchorLabel = getLocaleAnchorLabel($child)
                                        @endphp
                                        <li class="px-4 py-2 hover:cursor-pointer border-transparent">
                                            <a class="nav-text whitespace-nowrap block w-full text-sm xl:text-md {{ $child->class }}" href="{{ localeUrl($child->url) }}"
                                               target="{{ $child->target }}">
                                                {{ $childAnchorLabel }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </li>
                @endforeach

                @if(count(supportedLocales()) >= 2)
                    <li class="top-level relative whitespace-nowrap">
                        <a class="text-base text-right tracking-tight px-3 py-3 inline-block transition-all whitespace-nowrap" href="javascript:" data-target="menu-locales-1"
                           title="Select Currency">
                            {{ strtoupper(app()->getLocale()) }} <img class="down-carat svg-inject inline-block h-2 ml-2" src="{{ themeImage('down-carat.svg') }}" alt="&or;">
                        </a>
                        <div class="nav-submenu lg:absolute lg:top-8 hidden menu-locales-1 bg-white">
                            <ul class="border shadow no-drag">
                                @foreach(supportedLocales() as $locale)
                                    @if ($locale === app()->getLocale())
                                        @continue
                                    @endif
                                    <li class="px-4 py-2 text-gray-900  hover:bg-gray-200 hover:cursor-pointer border-transparent hover:bg-blue-500 hover:text-white">
                                        <a class="p-2 text-sm" href="{{ switchLocaleUrl(url()->current(), $locale) }}" rel="nofollow" title="Switch to {{ $locale }}">
                                            {{ strtoupper($locale) }} - {{ strtoupper(trans('languages.'.$locale)) }}
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </li>
                @endif

                @if(count($tenantCurrencies) > 1)
                    <li class="top-level relative whitespace-nowrap">
                        <a class="text-base text-right tracking-tight px-3 py-3 inline-block transition-all whitespace-nowrap" href="javascript:" data-target="menu-currencies"
                           title="Select Currency">
                            {{ $currentCurrency->iso }} <img class="down-carat svg-inject inline-block h-2 ml-2" src="{{ themeImage('down-carat.svg') }}" alt="&or;">
                        </a>
                        <div class="nav-submenu lg:absolute lg:top-8 hidden menu-currencies bg-white">
                            <ul class="border shadow no-drag">
                                @foreach($tenantCurrencies as $currencyIso)
                                    <li class="px-4 py-2 text-gray-900  hover:bg-gray-200 hover:cursor-pointer border-transparent hover:bg-blue-500 hover:text-white">
                                        <a class="p-2 text-sm" href="{{ localeUrl('/currency/'.strtolower($currencyIso)) }}" rel="nofollow"
                                           title="{{ Currency::getPropertyByIso('name', $currencyIso) }} Currency">
                                            {{ strtoupper($currencyIso) }} - {!! Currency::getPropertyByIso('symbol', $currencyIso) !!}
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </li>
                @endif

                <li class="block lg:hidden">
                    <div class="flex items-center justify-center">
                        <div>
                            <a class="flex items-center justify-center border border-white circle-button" href="{{ localeUrl('/contact') }}"><img
                                    class="h-3 svg-inject fill-current" src="{{ themeImage('email2.svg') }}" alt="email"></a>
                        </div>

                        <div class="mx-2">
                            <a class="flex items-center justify-center border border-white circle-button" href="tel:{{$siteTel}}"><img class="h-3 svg-inject fill-current"
                                                                                                                                       src="{{ themeImage('phone.svg') }}"
                                                                                                                                       alt="phone"></a>
                        </div>

                        <div>
                            @if(user())
                                <a class="flex items-center justify-center border border-white text-white circle-button"
                                   href="{{ localeUrl('/account') }}">
                                    <img src="/img/icons/person.svg" alt="" class="svg-inject fill-current stroke-current h-4">
                                </a>
                            @else
                                <a id="auth-button" class="modal-button flex items-center justify-center border border-white text-white circle-button" data-target="preauth-modal"
                                   href="javascript:">
                                    <img src="/img/icons/person.svg" alt="" class="svg-inject fill-current stroke-current h-4">
                                </a>
                            @endif
                        </div>
                    </div>
                </li>

                <li class="ml-2 hidden lg:block">
                    <a class="flex items-center justify-center border border-white circle-button" href="{{ localeUrl('/contact') }}"><img class="h-3 svg-inject fill-current"
                                                                                                                                          src="{{ themeImage('email2.svg') }}"
                                                                                                                                          alt="email"></a>
                </li>

                <li class="ml-2 hidden lg:block">
                    <a class="flex items-center justify-center border border-white circle-button" href="tel:{{$siteTel}}"><img class="h-3 svg-inject fill-current"
                                                                                                                               src="{{ themeImage('phone.svg') }}" alt="phone"></a>
                </li>

                @if(user())
                    <li class="ml-2 hidden lg:block">
                        <a class="flex items-center justify-center border border-white text-white circle-button"
                           href="{{ localeUrl('/account') }}">
                            <img src="/img/icons/person.svg" alt="" class="svg-inject fill-current stroke-current h-4">
                        </a>
                    </li>
                @else
                    <li class="ml-2 hidden lg:block">
                        <a id="auth-button" class="modal-button flex items-center justify-center border border-white text-white circle-button" data-target="preauth-modal"
                           href="javascript:">
                            <img src="/img/icons/person.svg" alt="" class="svg-inject fill-current stroke-current h-4">
                        </a>
                    </li>
                @endif
                {{-- @if (hasFeature(\App\Models\TenantFeature::FEATURE_VALUATION_SYSTEM))
                    <li class="mt-2 lg:mt-0">
                        <a class="valuation-button whitespace-nowrap lg:text-sm text-base text-center tracking-wide font-bold uppercase rounded-3xl max-w-xs block ml-4 py-1 lg:py-3 px-2 lg:px-8 transition-all cta"
                           href="{{ localeUrl('/valuation') }}">{{ trans('label.get_a_valuation') }}</a>
                    </li>
                @endif --}}
            </ul>
        </div>

        <div class="tham tham-e-squeeze tham-w-8 absolute right-4 top-1/2 lg:hidden z-10 transform -translate-y-1/2" id="menu">
            <div class="tham-box">
                <div class="tham-inner"/>
            </div>
        </div>

    </div>
    <div class="flex items-center justify-center absolute right-14 top-1/2 z-20 lg:hidden mobile-nav-icons transform -translate-y-1/2">
        <a href="{{ localeUrl('/contact') }}"><img class="h-4 mr-3 svg-inject fill-current" src="{{ themeImage('email2.svg') }}" alt="email"></a>
        <a href="tel:{{$siteTel}}"><img class="h-4 mr-3 svg-inject fill-current" src="{{ themeImage('phone.svg') }}" alt="phone"></a>
    </div>
</nav>

<header id="sticky-nav" class="navigation-bg -top-full opacity-0 transition-all fixed z-40 w-full duration-300 shadow px-4 lg:px-0">
    <div class="container mx-auto">
        <div class="mx-auto flex justify-between flex-col lg:flex-row relative py-2">

            <div class="relative">
                <a id="site-logo" href="{{ localeUrl('/') }}">
                    <img class="logo relative h-10 lg:h-12 z-10 @if (strtolower(getFileExtensionForFilename($logoPath)) === 'svg') svg-inject @endif" src="{{ $logoPath }}"
                         alt="logo" loading="lazy">
                </a>
            </div>
            <div class="hidden lg:block mobile-menu absolute lg:relative left-0 top-11 lg:top-0 lg:pb-0 pb-5 w-full lg:w-auto z-40 navigation-bg lg:bg-transparent">
                <ul class="flex flex-col lg:flex-row nav-text items-center">
                    @foreach ($navigation as $nav)
                        @php
                            $anchorLabel = getLocaleAnchorLabel($nav)
                        @endphp

                        <li class="top-level relative">
                            <a class="text-base text-right tracking-tight px-3 py-3 inline-block transition-all @if($nav->class !== null) {{ $nav->class }} @endif"
                               href="{{ $nav->children->count() ? 'javascript:;' : localeUrl($nav->url) }}" data-target="menu-{{ Str::slug($anchorLabel) }}">
                                {{ $anchorLabel }}
                                @if ($nav->children->count() > 0)
                                    <img class="down-carat svg-inject inline-block h-2 ml-2" src="{{ themeImage('down-carat.svg') }}" alt="&or;">
                                @endif
                            </a>
                            @if($nav->children->count() > 0)
                                <div class="relative lg:absolute lg:left-0 hidden lg:-translate-x-1/2 lg:top-10 menu-{{ Str::slug($anchorLabel) }}">
                                    <ul class="primary-bg text-gray-500 w-auto text-sm xl:text-md text-gray-900 lg:shadow no-drag dropdown-menu">
                                        @foreach ($nav->children as $child)
                                            @php
                                                $childAnchorLabel = getLocaleAnchorLabel($child)
                                            @endphp
                                            <li class="px-4 py-2 hover:cursor-pointer border-transparent">
                                                <a class="nav-text whitespace-nowrap block w-full text-sm xl:text-md {{ $child->class }}" href="{{ localeUrl($child->url) }}"
                                                   target="{{ $child->target }}">
                                                    {{ $childAnchorLabel }}
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>

                                </div>
                            @endif
                        </li>
                    @endforeach

                    @if(count($tenantCurrencies) > 1)
                        <li class="top-level relative whitespace-nowrap">
                            <a class="text-base text-right tracking-tight px-3 py-3 inline-block transition-all whitespace-nowrap" href="javascript:" data-target="menu-currencies"
                               title="Select Currency">
                                {{ $currentCurrency->iso }} <img class="down-carat svg-inject inline-block h-2 ml-2" src="{{ themeImage('down-carat.svg') }}" alt="&or;">
                            </a>
                            <div class="nav-submenu lg:absolute lg:top-8 hidden menu-currencies bg-white">
                                <ul class="border shadow no-drag">
                                    @foreach($tenantCurrencies as $currencyIso)
                                        <li class="px-4 py-2 text-gray-900  hover:bg-gray-200 hover:cursor-pointer border-transparent hover:bg-blue-500 hover:text-white">
                                            <a class="p-2 text-sm" href="{{ localeUrl('/currency/'.strtolower($currencyIso)) }}" rel="nofollow"
                                               title="{{ Currency::getPropertyByIso('name', $currencyIso) }} Currency">
                                                {{ strtoupper($currencyIso) }} - {!! Currency::getPropertyByIso('symbol', $currencyIso) !!}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </li>
                    @endif

                    <li class="block lg:hidden">
                        <div class="flex items-center justify-center">
                            <div>
                                <a class="flex items-center justify-center border border-white circle-button" href="{{ localeUrl('/contact') }}"><img
                                        class="h-3 svg-inject fill-current" src="{{ themeImage('email2.svg') }}" alt="email"></a>
                            </div>

                            <div class="mx-2">
                                <a class="flex items-center justify-center border border-white circle-button" href="tel:{{$siteTel}}"><img class="h-3 svg-inject fill-current"
                                                                                                                                           src="{{ themeImage('phone.svg') }}"
                                                                                                                                           alt="phone"></a>
                            </div>

                            <div>
                                @if(user())
                                    <a class="flex items-center justify-center border border-white text-white circle-button"
                                       href="{{ localeUrl('/account') }}">
                                        <img src="/img/icons/person.svg" alt="" class="svg-inject fill-current stroke-current h-4">
                                    </a>
                                @else
                                    <a id="auth-button" class="modal-button flex items-center justify-center border border-white text-white circle-button"
                                       data-target="preauth-modal" href="javascript:">
                                        <img src="/img/icons/person.svg" alt="" class="svg-inject fill-current stroke-current h-4">
                                    </a>
                                @endif
                            </div>
                        </div>
                    </li>

                    <li class="ml-2 hidden lg:block">
                        <a class="flex items-center justify-center border border-white circle-button" href="{{ localeUrl('/contact') }}"><img class="h-3 svg-inject fill-current"
                                                                                                                                              src="{{ themeImage('email2.svg') }}"
                                                                                                                                              alt="email"></a>
                    </li>

                    <li class="ml-2 hidden lg:block">
                        <a class="flex items-center justify-center border border-white circle-button" href="tel:{{$siteTel}}"><img class="h-3 svg-inject fill-current"
                                                                                                                                   src="{{ themeImage('phone.svg') }}" alt="phone"></a>
                    </li>

                    @if(user())
                        <li class="ml-2 hidden lg:block">
                            <a class="flex items-center justify-center border border-white text-white circle-button"
                               href="{{ localeUrl('/account') }}">
                                <img src="/img/icons/person.svg" alt="" class="svg-inject fill-current stroke-current h-4">
                            </a>
                        </li>
                    @else
                        <li class="ml-2 hidden lg:block">
                            <a id="auth-button" class="modal-button flex items-center justify-center border border-white text-white circle-button" data-target="preauth-modal"
                               href="javascript:">
                                <img src="/img/icons/person.svg" alt="" class="svg-inject fill-current stroke-current h-4">
                            </a>
                        </li>
                    @endif
                    {{-- @if (hasFeature(\App\Models\TenantFeature::FEATURE_VALUATION_SYSTEM))
                        <li class="mt-2 lg:mt-0">
                            <a class="valuation-button whitespace-nowrap lg:text-sm text-base text-center tracking-wide font-bold uppercase rounded-3xl max-w-xs block ml-4 py-1 lg:py-3 px-2 lg:px-8 transition-all cta"
                               href="{{ localeUrl('/valuation') }}">{{ trans('label.get_a_valuation') }}</a>
                        </li>
                    @endif --}}
                </ul>
            </div>

            <div class="tham tham-e-squeeze tham-w-8 absolute right-4 top-1/2 lg:hidden z-10 transform -translate-y-1/2" id="menu">
                <div class="tham-box">
                    <div class="tham-inner"/>
                </div>
            </div>

        </div>
        <div class="flex items-center justify-center absolute right-14 top-1/2 z-20 lg:hidden mobile-nav-icons transform -translate-y-1/2">
            <a href="{{ localeUrl('/contact') }}"><img class="h-4 mr-3 svg-inject fill-current" src="{{ themeImage('email2.svg') }}" alt="email"></a>
            <a href="tel:{{$siteTel}}"><img class="h-4 mr-3 svg-inject fill-current" src="{{ themeImage('phone.svg') }}" alt="phone"></a>
        </div>
    </div>
</header>
