@php
    $primaryGridCols = '';
    $searchControls = searchControlOptions(config('platform-cache.search_controls_enabled'));

    // define default visibility for select dropdowns
    $salePriceHidden = in_array($searchRequest->get('type'), ['rental', 'short-term-rental', 'long-term-rental']) ? 'hidden' : '';
    $rentalPriceHidden = !in_array($searchRequest->get('type'), ['rental', 'short-term-rental', 'long-term-rental']) ? 'hidden' : '';
    $currentCurrency = currentCurrency()
@endphp

<section>
    <div class="container mx-auto px-4">

            <div class="grid grid-cols-6 gap-0 py-4">

                <div class="col-span-6 lg:col-span-3 xl:col-span-3 text-center md:text-left pb-1 md:pb-0">
                    <span class="search-results-count text-lg leading-tight font-semibold text-primary">{!! $breadcrumbResultsString  !!} ({{ number_format($properties->total()) }})</span>
                </div>

                <div class="col-span-4 lg:col-span-2 xl:col-span-2 lg:text-right">

                    @include(themeViewPath('frontend.components.listings.view-modes'))

                    @if ($searchRequest->get('location') !== null)
                        <div class="inline-block px-0">
                            @if(user() === null)
                                <a href="javascript:" class="modal-button leading-normal font-medium text-sm mr-2" data-target="preauth-modal">
                                    <span class="leading-normal font-medium text-sm ">
                                        <img src="{{ themeImage('search/save.svg') }}" alt="share" class="svg-inject text-cta inline-block stroke-current mr-1">
                                        <span class="hidden xl:inline-block">{{ trans('search.save_search') }}</span>
                                    </span>
                                </a>
                            @else
                                @if ($savedSearch === null)
                                    <a class="save-this-search leading-normal font-medium text-sm mr-2"
                                       href="/account/searches/{{ base64_encode($_SERVER['REQUEST_URI']) }}">
                                        <img src="{{ themeImage('search/save.svg') }}" alt="share" class="svg-inject inline-block text-cta stroke-current">
                                        <span class="leading-normal font-medium text-sm"><span class="hidden xl:inline-block">{{ trans('search.save_search') }}</span></span>
                                    </a>
                                @else
                                    <a class="save-this-search leading-normal font-medium text-sm mr-2"
                                       href="/account/searches/{{ $savedSearch->uuid }}/delete">
                                        <img src="{{ themeImage('search/save.svg') }}" alt="share" class="svg-inject inline-block text-cta fill-current stroke-current">
                                        <span class="leading-normal font-medium text-sm"><span class="hidden xl:inline-block">{{ trans('search.saved_search') }}</span></span>
                                    </a>
                                @endif
                            @endif
                        </div>
                    @endif

                    <div class="inline-block px-0">
                        <a class="modal-button mr-2" href="javascript:" data-target="share-this-search-modal">
                            <img src="{{ themeImage('search/share.svg') }}" alt="share" class="inline-block svg-inject text-cta stroke-current fill-current">
                            <span class="text-primary leading-normal font-medium text-sm">
                                <span class="hidden xl:inline-block">{{ trans('search.share_this_search') }}</span>
                            </span>
                        </a>
                    </div>

                    {{-- @if($searchRequest->get('location') !== null)
                        @if(user())
                            <div class="inline-block px-0 sm:px-3 ">
                                <a class="modal-button mr-2" href="javascript:" data-target="create-alert-modal">
                                    <img src="{{ themeImage('search/bell.svg') }}" alt="bell" class="inline-block svg-inject text-cta stroke-current fill-current">
                                    <span class="text-primary leading-normal font-medium text-sm"><span
                                            class="hidden xl:inline-block">{{ trans('search.create_property_alert') }}</span></span>
                                </a>
                            </div>
                        @else
                            <div class="inline-block px-0 sm:px-3 ">
                                <a class="modal-button mr-2" href="javascript:" data-target="preauth-modal">
                                    <img src="{{ themeImage('search/bell.svg') }}" alt="bell" class="inline-block svg-inject text-cta stroke-current fill-current">
                                    <span class="text-primary leading-normal font-medium text-sm"><span
                                            class="hidden xl:inline-block">{{ trans('search.create_property_alert') }}</span></span>
                                </a>
                            </div>
                        @endif
                    @endif --}}

                </div>

                <div class="col-span-2 lg:col-span-1 xl:col-span-1 text-right">
                    <select id="sort" name="sort" class="autojump w-auto xl:w-32 block focus:outline-none bg-white leading-normal font-medium text-md float-right appearance-none select-carat">
                        <option value="">{{ trans('sort.sort_by') }}</option>
                        <option value="{{ getSortByUrl('most-recent') }}" @if($searchRequest->get('sort') === 'most-recent') selected @endif>{{ trans('sort.most_recent') }}</option>
                        <option value="{{ getSortByUrl('price-low') }}" @if($searchRequest->get('sort') === 'price-low') selected @endif>{{ trans('sort.lowest_price') }}</option>
                        <option value="{{ getSortByUrl('price-high') }}" @if($searchRequest->get('sort') === 'price-high') selected @endif>{{ trans('sort.highest_price') }}</option>
                    </select>
                </div>

            </div>
    </div>
</section>
