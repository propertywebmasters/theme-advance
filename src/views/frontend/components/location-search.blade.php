@php
    if (!isset($additionalClasses)) {
        $additionalClasses = '';
    }

    if (!isset($mobile)) {
        $mobile = false;
    }

    if (!isset($refinedSearch)) {
        $refinedSearch = false;
    }

    if (!isset($areaGuide)) {
        $areaGuide = false;
    }

    if (!isset($value)) {
        $value = '';
    }

    if (!isset($searchResultsContainer)) {
        $searchResultsContainer = 'search-results-container'; 
    }

    if (!isset($placeholder)) {
        $placeholder = trans('placeholder.search_location');
    }
@endphp

@if(isset($searchControls->location) && $searchControls->location)
    @if(hasFeature(\App\Models\TenantFeature::FEATURE_USE_LOCATION_DATALIST))
        @if ($mobile)
            @include(themeViewPath('frontend.components.location-datalist-refine-mobile'))
        @elseif ($refinedSearch)
            <div class="lg:col-span-2 bg-white p-4 col-span-2 relative {{ $additionalClasses }}">
                @include(themeViewPath('frontend.components.location-datalist-refine-desktop'))
            </div>
        @else
            @include(themeViewPath('frontend.components.location-datalist'), ['areaGuide' => $areaGuide])
        @endif
    @elseif ($areaGuide)
        @include(themeViewPath('frontend.components.location-search-input'), ['value' => $value, 'additionalClasses' => $additionalClasses, 'placeholder' => $placeholder])

        <div id="autocomplete-results" class="click-outside absolute top-0 w-full -ml-4 z-20 top-full" style="z-index: 100;"
                data-classes="py-1 px-4 border bg-white hover:bg-gray-200 text-gray-800 cursor-pointer">
            <ul class="desktop-search-results-container"></ul>
        </div>
        <input type="hidden" name="location_url" value="">
    @elseif ($refinedSearch)
        <div class="lg:col-span-2 bg-white p-4 col-span-2 relative {{ $additionalClasses }}">
            @include(themeViewPath('frontend.components.location-search-input'), ['value' => $value, 'additionalClasses' => $inputClasses])

            <input type="hidden" name="location_url" value="">
            <div id="autocomplete-results" class="click-outside absolute top-12 w-full z-30"
                data-classes="py-1 px-4 border bg-white hover:bg-gray-200 text-gray-800 cursor-pointer">
                <ul class="search-results-container"></ul>
            </div>
        </div>
    @elseif ($mobile)
        @include(themeViewPath('frontend.components.location-search-input'), [
            'additionalClasses' => $additionalClasses, 
            'searchResultsContainer' => $searchResultsContainer,
            'value' => $value,
        ])

        <input type="hidden" name="location_url" value="">
    @else
        {{-- @php
            $roundedClass = '';
            $borderClass = '';

            if ($searchControls->tender_type === 'sale_only') {
                $roundedClass = 'rounded-t-lg lg:rounded-full lg:rounded-r-none';
            }

            if (isset($searchControls->tender_type) && $searchControls->tender_type !== 'sale_only') {
                $borderClass .= 'lg:border-l ';
            }

            if (isset($searchControls->property_type) && $searchControls->property_type) {
                $borderClass .= 'lg:border-r ';
            }

            $additionalClasses = 'h-8 px-4 text-base';
            $additionalClasses .= $roundedClass ?? 'lg:rounded-l-none';
        @endphp

        <div class="lg:col-span-3" style="background-color: transparent;">
            <div class="w-full h-full pr-2 {{ $roundedClass }}" style="background-color: white;">
                <div class="pr-2 py-2 {{ $roundedClass }}">
                    <div class="pr-3 relative {{ $borderClass }} {{ $roundedClass }}">
                        @include(themeViewPath('frontend.components.location-search-input'), ['value' => $value, 'additionalClasses' => $additionalClasses, 'placeholder' => $placeholder])

                        <input type="hidden" name="location_url" value="">
                        <div id="autocomplete-results" class="click-outside absolute top-10 w-full z-30"
                                data-classes="py-1 px-4 border bg-white hover:bg-gray-200 text-gray-800 text-left cursor-pointer">
                            <ul class="search-results-container"></ul>
                        </div>
                    </div>
                </div>
            </div>
        </div> --}}

        @php
            $colSpan = 3;

            if (isset($searchControls->tender_type) && $searchControls->tender_type !== 'both') {
                $colSpan = 4;
            }
        @endphp

        <div class="col-span-2 lg:col-span-{{ $colSpan }} bg-white p-4 relative rounded-full">
            
            @include(themeViewPath('frontend.components.location-search-input-home'), ['value' => $value, 'additionalClasses' => $additionalClasses, 'placeholder' => $placeholder])

            <div id="autocomplete-results" class="click-outside absolute top-0 w-full mt-12 -ml-4 z-20" style="z-index: 100;"
                    data-classes="py-1 px-4 border bg-white hover:bg-gray-200 text-gray-800 cursor-pointer">
                <ul class="desktop-search-results-container"></ul>
            </div>
            <input type="hidden" name="location_url" value="">
        </div>
    @endif
@endif
