<div class="swiper-slide py-12 flex-shrink-0">
    <div class="bg-white p-6 shadow-lg rounded-lg text-center min-h-60">
        <p class="text-lg leading-tight text-center tracking-tight line-clamp-6">“{!! $testimonial->content !!}“</p>
        <span class="text-sm text-center tracking-tight font-bold mt-3 inline-block">{{ $testimonial->company_name !== null ? $testimonial->name . ', ' . $testimonial->company_name : $testimonial->name }}</span>
    </div>
</div>