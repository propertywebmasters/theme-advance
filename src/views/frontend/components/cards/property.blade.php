@php
    use App\Models\Property;use App\Models\TenantFeature;$splitMapMode = $splitMapMode ?? false;

    $class = $data = '';
    if (!isset($extraCss)) {
        $extraCss = '';
    }
    if ($splitMapMode) {
        $propertyImages = $property->images->pluck('filepath');
        $class = 'map-listing cursor-pointer';
        $data =  'data-zoom="12" data-latitude="'.$property->location->latitude.'" data-longitude="'.$property->location->longitude.'"';
    } else {
        $propertyImages = propertyImagesArray($property);
        $data =  'data-gallery_images="'.propertyImagesString($property).'"';
    }

    if (!isset($thumbnailSize)) {
        $thumbnailSize = 'md';
    }
@endphp

<div class="relative rounded-lg property-listing custom-inline-listing-gallery z-1 {{ $class }} {{ $extraCss }}" {{ $data }}>
    <div class="relative min-h-76 h-76 max-h-76 overflow-hidden">
        <div class="absolute left-0 bottom-0 px-4 rounded-tr-lg z-10" style="background: var(--ap-cta-bg-transparent);">
            <span class="property-price text-lg leading-loose font-bold block text-white">
                {!! $property->displayPrice() !!}
            </span>
        </div>

        @if($property->tender_status !== Property::TENDER_STATUS_AVAILABLE)
            <div class="absolute tender-status top-2 right-2 primary-bg text-white p-2">{{ $property->tender_status }}</div>
        @endif

        @php
            $imageUrl = !isset($propertyImages[0]) || $propertyImages[0] === null ? '/img/image-coming-soon.jpg' : $propertyImages[0];
            $propertyImage = getPropertyImage($imageUrl);
            $bgImageClass = 'min-height: 300px; overflow: hidden; background-position: center center; background-size: cover; width: 100%; cursor: pointer; background-image: url("'.$propertyImage.'")';
        @endphp

        <div id="property-{{ $property->uuid }}" class="grid grid-cols-1 lg:grid-cols-5 gap-1 relative h-full">
            @if(hasFeature(TenantFeature::FEATURE_SHORTLIST_SYSTEM))
                @if (user() === null)
                    @php
                        $actionClass = 'modal-button';
                        $dataAttribs = 'data-target="preauth-modal"';
                        $imgClass = '';
                    @endphp
                @else
                    @php
                        if (in_array($property->uuid, $shortlistedProperties->pluck('property_uuid')->toArray())) {
                            // this property is in the shortlist
                            $actionClass = 'shortlist-toggle-simple';
                            $dataAttribs = 'data-on-class="primary-text fill-current stroke-current" data-property="'.$property->uuid.'"';
                            $imgClass = 'primary-text fill-current stroke-current';
                        } else {
                            // this property is in the shortlist
                            $actionClass = 'shortlist-toggle-simple';
                            $dataAttribs = 'data-on-class="primary-text fill-current stroke-current" data-property="'.$property->uuid.'"';
                            $imgClass = '';
                        }
                    @endphp
                @endif
                <div class="inline-block absolute top-4 right-4 z-10" style="width: 40px; height: 40px;">
                    <a href="javascript:" class="{{ $actionClass }} bg-white rounded-full h-8 w-8 shadow-lg block h-full w-full flex items-center justify-center"
                       title="{{ trans('shortlist.save') }}" {!! $dataAttribs !!}>
                        <img src="{{ themeImage('heart.svg') }}" class="svg-inject {{ $imgClass }} inline-block h-5" alt="love" loading="lazy">
                    </a>
                </div>

                @if($property->tender_status !== Property::TENDER_STATUS_AVAILABLE)
                    <div class="absolute tender-status top-2 left-2 primary-bg text-white p-2 rounded-l-2xl rounded-r-2xl z-10">{{ $property->tender_status }}</div>
                @endif

            @endif

            <div class="col-span-1 lg:col-span-3 relative">
                <div class="rounded-tl-lg bg-lazy-load img-1 rounded-tr-lg lg:rounded-tr-none h-full" style="{{ $bgImageClass }}" data-style="{{ $bgImageClass }}"
                     onclick="window.location='{{ localeUrl('/property/'.$property->url_key) }}'" data-current-image-src="{{ $propertyImage }}">&nbsp;
                </div>

                @if(hasFeature(TenantFeature::FEATURE_LISTINGS_INLINE_GALLERY) && !$splitMapMode && $property->images->count() > 1)
                    <div class="inline-gallery-control hidden previous-image p-4 absolute left-0 top-1/2 text-white cursor-pointer transform -translate-y-1/2"
                         data-target="property-{{ $property->uuid }}">
                        <img class="inline-block svg-inject text-white fill-current stroke-current h-6 p-0 m-0" src="{{ themeImage('icons/left-carat.svg') }}" loading="lazy">
                    </div>
                    <div class="inline-gallery-control hidden next-image p-4 absolute right-0 top-1/2 text-white cursor-pointer transform -translate-y-1/2"
                         data-target="property-{{ $property->uuid }}">
                        <img class="inline-block svg-inject text-white fill-current stroke-current h-6 p-0 m-0" src="{{ themeImage('icons/right-carat.svg') }}" loading="lazy">
                    </div>
                @endif
            </div>
            <div class="hidden lg:grid col-span-2 gap-1 h-full" style="min-height: 300px;">
                @php
                    $imageUrl = $propertyImages[1] ?? '/img/image-coming-soon.jpg';
                    $bgImageClass = 'background-image: url("'.getPropertyImage($imageUrl).'");';
                    $classes = 'bg-lazy-load w-full h-full overflow-hidden bg-center bg-cover cursor-pointer'
                @endphp

                <div class="rounded-tr-lg img-2 {{ $classes }}" style="{{ $bgImageClass }}" data-style="{{ $bgImageClass }}"
                     onclick="window.location='{{ localeUrl('/property/'.$property->url_key) }}'">&nbsp;
                </div>

                @php
                    $imageUrl = $propertyImages[2] ?? '/img/image-coming-soon.jpg';
                    $bgImageClass = 'background-image: url("'.getPropertyImage($imageUrl).'");'
                @endphp

                <div class="img-3 {{ $classes }}" style="{{ $bgImageClass }}" data-style="{{ $bgImageClass }}"
                     onclick="window.location='{{ localeUrl('/property/'.$property->url_key) }}'">&nbsp;
                </div>
            </div>
        </div>
    </div>

    <div class="relative px-5 lg:px-7 md:py-7 pt-4 pb-7 shadow-md rounded-lg">
        <div class="grid grid-cols-4 sm:grid-cols-3 lg:grid-cols-7 items-center">
            <div class="col-span-2 lg:col-span-4 pr-4 border-r">
                <a class="text-xl text-primary m-0 p-0 overflow-hidden block line-clamp-1 font-bold tracking-tight"
                   href="{{ localeUrl('/property/'.$property->url_key) }}">{!! $property->displayName() !!}</a>
                <span class="text-sm tracking-tight line-clamp-1 text-gray-400">{!! $property->location->displayAddress() !!} </span>
            </div>

            <div class="col-span-2 sm:col-span-1 lg:col-span-3">
                @include(themeViewPath('frontend.components.property.bed-bath-info'))
            </div>
        </div>
    </div>

</div>

{{--
<div class="relative rounded-lg property-listing inline-listing-gallery z-1 {{ $class }} {{ $extraCss }}" {{ $data }}>
    <div class="relative h-76 xl:h-100 overflow-hidden">

        @if($property->tender_status !== \App\Models\Property::TENDER_STATUS_AVAILABLE)
            <div class="absolute tender-status top-2 right-2 primary-bg text-white p-2 rounded-l-2xl rounded-r-2xl">{{ $property->tender_status }}</div>
        @endif

        @php
            $imageUrl = $property->images->first() === null ? '/img/image-coming-soon.jpg' : $property->images->first()->filepath;
            $bgImageClass = 'height: 100%; overflow: hidden; background-position: center; background-size: cover; width: 100%; cursor: pointer; background-image: url("'.getPropertyImage($imageUrl, $thumbnailSize).'")';
        @endphp

        <div class="rounded-t-lg bg-lazy-load" style="{{ $bgImageClass }}" data-style="{{ $bgImageClass }}" id="property-{{ $property->uuid }}" onclick="window.location='{{ localeUrl('/property/'.$property->url_key) }}'">&nbsp;</div>

        @if(hasFeature(\App\Models\TenantFeature::FEATURE_LISTINGS_INLINE_GALLERY) && !$splitMapMode && $property->images->count() > 1)
            <div class="inline-gallery-control hidden previous-image p-4 absolute left-0 top-1/2 text-white cursor-pointer transform -translate-y-1/2" data-target="property-{{ $property->uuid }}">
                <img class="inline-block svg-inject text-white fill-current stroke-current h-6 p-0 m-0" src="{{ themeImage('icons/left-carat.svg') }}" loading="lazy">
            </div>
            <div class="inline-gallery-control hidden next-image p-4 absolute right-0 top-1/2 text-white cursor-pointer transform -translate-y-1/2" data-target="property-{{ $property->uuid }}">
                <img class="inline-block svg-inject text-white fill-current stroke-current h-6 p-0 m-0" src="{{ themeImage('icons/right-carat.svg') }}" loading="lazy">
            </div>
        @endif

        @if(hasFeature(\App\Models\TenantFeature::FEATURE_SHORTLIST_SYSTEM))
            @if (user() === null)
                @php
                $actionClass = 'modal-button';
                $dataAttribs = 'data-target="preauth-modal"';
                $imgClass = '';
                @endphp
            @else
                @php
                    if (in_array($property->uuid, $shortlistedProperties->pluck('property_uuid')->toArray())) {
                        // this property is in the shortlist
                        $actionClass = 'shortlist-toggle-simple';
                        $dataAttribs = 'data-on-class="primary-text fill-current stroke-current" data-property="'.$property->uuid.'"';
                        $imgClass = 'primary-text fill-current stroke-current';
                    } else {
                        // this property is in the shortlist
                        $actionClass = 'shortlist-toggle-simple';
                        $dataAttribs = 'data-on-class="primary-text fill-current stroke-current" data-property="'.$property->uuid.'"';
                        $imgClass = '';
                    }
                @endphp
            @endif
            <div class="inline-block absolute top-4 right-4" style="width: 40px; height: 40px;">
                <a href="javascript:" class="{{ $actionClass }} bg-white rounded-full h-8 w-8 shadow-lg block h-full w-full flex items-center justify-center" title="{{ trans('shortlist.save') }}" {!! $dataAttribs !!}>
                    <img src="{{ themeImage('heart.svg') }}" class="svg-inject {{ $imgClass }} inline-block h-5" alt="love" loading="lazy">
                </a>
            </div>
        @endif

        <div class="absolute left-0 bottom-0 px-4 rounded-tr-lg" style="background: var(--ap-cta-bg);">
            <span class="property-price text-xl leading-loose font-bold uppercase block text-white">
                {!! $property->displayPrice() !!}
            </span>
        </div>
    </div>
    <div class="relative px-5 lg:px-7 md:py-7 pt-4 pb-7 shadow-md rounded-lg">
        <div class="grid grid-cols-4 sm:grid-cols-3 lg:grid-cols-7 items-center">
            <div class="col-span-2 lg:col-span-4 pr-4 border-r">
                <a class="text-xl text-primary m-0 p-0 overflow-hidden block line-clamp-1 font-bold tracking-tight" href="{{ localeUrl('/property/'.$property->url_key) }}">{!! $property->displayName() !!}</a>
                <span class="text-sm tracking-tight line-clamp-1 text-gray-400">{!! $property->location->displayAddress()  !!} </span>
            </div>

            <div class="col-span-2 sm:col-span-1 lg:col-span-3">
                @include(themeViewPath('frontend.components.property.bed-bath-info'))
            </div>
        </div>
    </div>

</div>  --}}
