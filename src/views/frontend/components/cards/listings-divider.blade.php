@if(hasFeature(\App\Models\TenantFeature::FEATURE_ACCOUNT_SYSTEM))
    @if ($cardListingsDivider)
        @php
        if (preg_match('/^#[a-f0-9]{6}$/i', optional($cardListingsDivider)->background)) {
            $bgColorOrImage = "background-color: {$cardListingsDivider->background}";
        } else {
            $bgColorOrImage = "background: url('" . assetPath($cardListingsDivider->background ) . "'); background-size: cover; background-position: center;";
        }
        @endphp

        @if ($list)
            <div class="text-left text-white relative listings-divider mb-11 rounded-lg" style="{{ $bgColorOrImage }}">
                @if (!preg_match('/^#[a-f0-9]{6}$/i', optional($cardListingsDivider)->background))
                    <div class="overlay w-full h-full absolute bg-black opacity-30 rounded-lg"></div>
                @endif

                <div class="p-8 sm:p-10 relative grid grid-cols-1 md:grid-cols-3 items-center">
                    <div class="col-span-2 mb-4 md:mb-0">
                        <h1 class="font-bold text-4xl mb-6">{{ $cardListingsDivider->title }}</h1>
                        <p>{{ $cardListingsDivider->text }}</p>
                    </div>

                    <div class="col-span-1">
                        @foreach ($cardListingsDivider->links as $link)
                            <div class="{{ $loop->first ? 'mt-0' : 'mt-4' }}">
                                <a class="py-2 cta block text-center rounded-lg" href="{{ $link->link }}">{{ $link->label }}</a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        @else
            <div class="w-full h-full text-center text-white flex items-center justify-center relative listings-divider rounded-lg" style="{{ $bgColorOrImage }}">
                @if (!preg_match('/^#[a-f0-9]{6}$/i', optional($cardListingsDivider)->background))
                    <div class="overlay w-full h-full absolute bg-black opacity-30 rounded-lg"></div>
                @endif

                <div class="px-8 sm:px-4 lg:px-10 py-8 sm:py-0 relative">
                    <h1 class="font-bold text-4xl mb-6">{{ $cardListingsDivider->title }}</h1>
                    <p>{{ $cardListingsDivider->text }}</p>

                    @foreach ($cardListingsDivider->links as $link)
                        <div class="{{ $loop->first ? 'mt-4' : 'mt-2' }}">
                            <a class="py-2 cta block rounded-lg" href="{{ $link->link }}">{{ $link->label }}</a>
                        </div>
                    @endforeach
                </div>
            </div>
        @endif
    @endif
@endif
