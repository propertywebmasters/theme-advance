<!-- multi-select-refine-desktop -->
<div class="multi-property-type-selector relative w-full block focus:outline-none -ml-1 bg-white lg:pl-2 border-l-0 pl-0 text-base lg:text-sm xl:text-base appearance-none select-carat rounded-none border-b-0 border-t-0 border-right-0">

    <span class="multi-property-type-initial-value line-clamp-1 text-sm xl:text-base" data-initial-text="{{ trans('search.select_property_types') }}" data-selected-text="{{ trans('search.property_types_selected') }}">
        @if(isset($searchRequest) && is_array($searchRequest->get('property_type')) && count($searchRequest->get('property_type')) > 0) {{ count($searchRequest->get('property_type')) }} {{ trans('search.property_types_selected') }}  @else {{ trans('label.property_type') }} @endif
    </span>

    <div class="multi-property-type-options-list hidden absolute w-full top-16 left-0 click-outside-multi-select bg-white p-4 z-20 rounded-b-sm">
        <div class="multi-property-type-options grid grid-cols-2 xl:grid-cols-3 gap-4">

            @if(hasFeature(\App\Models\TenantFeature::FEATURE_USE_HEIRARCHICAL_CATEGORIES))
                @foreach($propertyTypes as $propertyType)
                    @php $active = isset($searchRequest) && is_array($searchRequest->get('property_type')) && in_array($propertyType->url_key, $searchRequest->get('property_type')) @endphp
                    <div class="text-sm @if($active) active @endif relative"><span class="@if($active) active @endif rounded-sm inline-block w-3 h-3 border border-white text-white multi-property-type-option mr-2" data-value="{{ $propertyType->url_key }}"  style="vertical-align: middle;"></span> {{ transPropertyType($propertyType->name) }}</div>
                @endforeach
            @else
                @foreach($propertyTypeCategories as $category)
                    @php $active = isset($searchRequest) && is_array($searchRequest->get('property_type')) && in_array($category['category_slug'], $searchRequest->get('property_type')) @endphp
                    <div class="text-sm @if($active) active @endif relative"><span class="@if($active) active @endif rounded-sm inline-block w-3 h-3 border border-white text-white multi-property-type-option mr-2" data-value="{{ $category['category_slug'] }}"  style="vertical-align: middle;"></span> {{ transPropertyType($category['category']) }}</div>
                @endforeach
            @endif

        </div>
    </div>

    <div class="multi-property-type-hidden-input-wrapper"><input type="hidden" name="property_type"></div>

</div>
