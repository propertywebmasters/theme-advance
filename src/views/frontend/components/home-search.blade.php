@php
    use App\Models\TenantFeature;$searchControls = searchControlOptions(config('platform-cache.search_controls_enabled'));
    $absolutelyPosition = $absolutelyPosition ?? false;
@endphp

<div id="home-search-container" class="@if($absolutelyPosition) w-full absolute top-1/3 lg:top-1/2 z-30 @endif">
    <form id="home-search" action="{{ localeUrl('/search') }}" method="post" enctype="application/x-www-form-urlencoded"
          @if($absolutelyPosition) class="mx-auto w-4/5 xl:w-3/5" @endif>
        @php
            $colSpan = 2;

            if (isset($searchControls->tender_type) && $searchControls->tender_type !== 'both') {
                $colSpan = 3;
            }
        @endphp

        <div class="grid grid-cols-{{ $colSpan }} lg:grid-cols-5 w-full relative gap-2">

            @include(themeViewPath('frontend.components.location-search'), ['value' => $location ?? ''])

            @if(isset($searchControls->tender_type) && $searchControls->tender_type === 'both')
                <div class="col-span-1">
                    <button type="submit" name="type" value="sale"
                            class="min-h-12 block text-lg text-white font-bold w-full h-full focus:outline-none cursor-pointer cta tracking-wider rounded-full">
                        {{ trans('generic.buy') }}
                    </button>
                </div>

                <div class="col-span-1">
                    <button type="submit" name="type" value="rental"
                            class="min-h-12 block text-lg text-white font-bold w-full h-full focus:outline-none cursor-pointer primary-bg tracking-wider rounded-full">
                        {{ trans('generic.rent') }}
                    </button>
                </div>
            @elseif(isset($searchControls->tender_type) && ($searchControls->tender_type === 'sale_only' || $searchControls->tender_type === 'rental_only'))
                @if($searchControls->tender_type === 'sale_only')
                    <div class="col-span-1">
                        <button type="submit" name="type" value="sale"
                                class="min-h-12 block text-lg text-white font-bold w-full h-full focus:outline-none cursor-pointer cta tracking-wider rounded-full">
                            {{ trans('generic.buy') }}
                        </button>
                    </div>
                @elseif($searchControls->tender_type === 'rental_only')
                    <div class="col-span-1">
                        <button type="submit" name="type" value="rental"
                                class="min-h-12 block text-lg text-white font-bold w-full h-full focus:outline-none cursor-pointer primary-bg tracking-wider rounded-full">
                            {{ trans('generic.rent') }}
                        </button>
                    </div>

                    {{-- <div class="bg-white p-4 col-span-2 sm:col-span-1">
                        <label for="search_type" class="text-xs">{{ trans('label.for') }}</label>
                        <select id="search_type" name="type" class="w-full block focus:outline-none bg-transparent appearance-none select-carat">
                            <option value="rental">{{ trans('label.rental') }}</option>
                            @if($hasShortTermRentals)
                                <option value="short_term_rental">{{ trans('label.short_term_rental') }}</option>
                            @endif
                            @if($hasLongTermRentals)
                                <option value="long_term_rental">{{ trans('label.long_term_rental') }}</option>
                            @endif
                        </select>
                    </div> --}}
                @endif
            @endif

            <input type="hidden" name="locale" value="{{ app()->getLocale() }}">
            @if(hasFeature(TenantFeature::FEATURE_DEVELOPMENT_SEARCH_ON_HOMEPAGE))
                <input type="hidden" name="is_development" value="1">
            @endif

        </div>
    </form>
</div>
