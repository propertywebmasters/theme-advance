@if ($popularSearches->count() > 0)
    <section id="popular-searches" class="mb-10 lg:mb-24 py-12">
        <div class="container mx-auto px-4 md:px-0">

            <h3 class="header-text text-2xl md:text-3xl text-left tracking-tight text-primary mb-4">{{ trans('search.how_can_we_help_you') }}</h3>

            <div>
                <div class="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-4 gap-8 lg:gap-4 xl:gap-8">
                    @foreach($popularSearches as $search)
                        <div class="popular-search-location relative overflow-hidden rounded-lg h-100 xl:h-108">
                            <img src="{{ assetPath($search->image) }}" class="object-cover w-full h-full" loading="lazy">
                            <div class="popular-search-location-item absolute bottom-0 left-0 w-full text-center px-8 transition-all duration-300 flex flex-col items-center justify-center h-16" style="background: var(--ap-primary-transparent);">
                                <div>
                                    <p href="{{ $search->url }}" class="text-lg text-white py-4 px-4 font-bold">{!! $search->title !!}</p>
                                </div>

                                <div>
                                    <a href="{{ $search->url }}" class="view-more-button border border-white rounded-full px-10 py-4 text-white invisible hidden transition-all duration-500 capitalize-first-letter text-sm font-bold opacity-0">
                                        {{ trans('generic.view_more') }}
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
@endif