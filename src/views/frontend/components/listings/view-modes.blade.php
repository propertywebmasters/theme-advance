@php
    use App\Models\TenantFeature;
    $hasListView = hasFeature(TenantFeature::FEATURE_LIST_VIEW);
    $hasSplitView = hasFeature(TenantFeature::FEATURE_SPLIT_MAP_VIEW);

    $modes = 1;
    if ($hasListView) { $modes++; }
    if ($hasSplitView) { $modes++; }

    $listModeSelected = request()->get('list') == 1;
    $mapModeSelected = request()->has('map') == 1;
    $gridModeSelected =  !$listModeSelected && !$mapModeSelected
@endphp

@if ($modes > 1)
    <div class="inline-block px-0">

        <a href="{{ gridModeUrl() }}" class="mr-2">
            <img src="{{ themeImage('search/grid.svg') }}" alt="grid"
                 class="@if($gridModeSelected) text-cta @else text-gray-300 @endif inline-block svg-inject stroke-current fill-current" loading="lazy">
        </a>

        @if($hasListView)
            <a href="{{ listModeUrl() }}" class="mr-2">
                <img src="{{ themeImage('search/list.svg') }}" alt="list"
                     class="@if($listModeSelected) text-cta @else text-gray-300 @endif inline-block svg-inject stroke-current fill-current" loading="lazy">
            </a>
        @endif

        @if($hasSplitView)
            <a href="{{ mapModeUrl() }}" class="mr-2 hidden xl:inline-block">
                <img src="{{ themeImage('search/pin.svg') }}" alt="map"
                     class="@if($mapModeSelected) text-cta @else text-gray-300 @endif inline-block svg-inject fill-current stroke-current" loading="lazy">
            </a>
        @endif

    </div>
@endif
