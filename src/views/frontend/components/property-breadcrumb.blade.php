@php
$breadcrumbs[] = [trans('header.home') => '/'];

$locationSlug = strtolower(urlencode($property->location->city));

if ($property->tender_type === 'sale') {
    $breadcrumbs[] = [ucwords($property->location->city) => '/all-properties-for-sale/in/'.$locationSlug];
} else {
    $breadcrumbs[] = [ucwords($property->location->city) => '/all-properties-for-rent/in/'.$locationSlug];
}

$breadcrumbs[] = [$property->displayName() => '#']

@endphp

@php $i = 1 @endphp
@foreach ($breadcrumbs as $breadcrumb)
    @foreach($breadcrumb as $anchor => $link)
            <a href="{{ localeUrl($link) }}" class="text-sm" style="color: #575757;">{{ $anchor }}</a>
        @if($i < count($breadcrumbs))
            <span class="text-sm text-gray-400">&gt;</span>
        @endif
        @php $i++ @endphp
    @endforeach
@endforeach
