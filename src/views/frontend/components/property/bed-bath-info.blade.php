@php
    $showTextSmallerScreens = $showTextSmallerScreens ?? false;
    $showSizing = $showSizing ?? false;
@endphp

<div id="bed-bath-info" class="flex justify-end">
    @if ($property->isHouse())
        @if($property->bedrooms != 0)
        <div class="flex items-center mr-3 lg:mr-2">
            <img style="height: 0.625rem; width: auto;" src="{{ themeImage('bed.svg') }}" alt="bed" loading="lazy" class="text-cta fill-current svg-inject">
            <span class="ml-2 text-grey text-sm">{{ $property->bedrooms }} <span class="text-sm {{ $showTextSmallerScreens ? '' : 'hidden sm:inline md:hidden lg:inline' }}">{{ trans('generic.bed') }}</span></span>
        </div>
        @endif
        <div class="flex items-center mr-3 lg:mr-2">
            <img style="height: 0.75rem; width: auto;" src="{{ themeImage('bath.svg') }}" alt="bath" loading="lazy" class="text-cta fill-current svg-inject">
            <span class="ml-2 text-grey text-sm">{{ str_replace('.0', '', $property->bathrooms) }} <span class="text-sm {{ $showTextSmallerScreens ? '' : 'hidden sm:inline md:hidden lg:inline' }}">{{ trans('generic.bath') }}</span></span>
        </div>
    @endif

    @if($showSizing)
        @if($property->internal_size !== null)
            @php $unit = getAreaUnitSymbol($property->area_unit); @endphp
            <div class="flex items-center mr-3 lg:mr-2">
                <img src="{{ themeImage('internal-size.svg') }}" alt="resize" loading="lazy" class="text-cta fill-current stroke-current svg-inject h-4 mt-0.5">
                <span class="inline-block ml-2 text-grey text-base">{{ $property->internal_size }} {!! $unit !!}</span>
            </div>
        @endif
        @if($property->land_size !== null)
            @php $unit = getAreaUnitSymbol($property->area_unit); @endphp
            <div class="flex items-center">
                <img src="{{ themeImage('land-size.svg') }}" alt="resize" loading="lazy" class="text-cta fill-current stroke-current svg-inject h-4 mt-0.5">
                <span class="inline-block ml-2 text-grey text-base">{{ $property->land_size }} {!! $unit !!}</span>
            </div>
        @endif
    @endif

</div>
