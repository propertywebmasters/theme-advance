@php
    use App\Models\TenantFeature;

    $slideSecondaryTitleEnabled = hasFeature(TenantFeature::FEATURE_SLIDE_SECONDARY_TITLE);
    $slideSubtitlesEnabled = hasFeature(TenantFeature::FEATURE_SLIDE_SUBTITLES);
@endphp

<div class="h-full">
    <div class="lg:flex lg:justify-between items-center align-items-center lg:flex-row h-full">

        <div class="w-full text-left absolute px-4 lg:px-0 lg:mt-16 top-1/2 transform -translate-y-1/2 z-20 lg:pl-10">
            {{-- <h1 class="text-4xl xl:text-4xl tracking-tight header-text primary-text mb-4 lg:mb-9 line-clamp-2 pr-0 lg:pr-8 ml-1 lg:pl-2 text-white lg:text-black">{!! $video->title !!}</h1> --}}

            <div class="mb-2">
                <h1 id="slide-title" class="text-5xl tracking-tight header-text primary-text mb-2 text-white">{!! $video->title !!}</h1>

                @if ($slideSecondaryTitleEnabled && $video->secondary_title)
                    <h1 id="slide-secondary-title" class="text-6xl xl:text-7xl primary-text secondary-header-text">{!! $video->secondary_title !!}</h1>
                @endif
            </div>

            @if ($slideSubtitlesEnabled && $video->subtitle)
                <h3 id="slide-subtitle" class="block pb-6 text-lg text-white mb-10 pt-4">{!! $video->subtitle !!}</h3>
            @endif

            <div class="flex flex-col lg:flex-row lg:items-center justify-center lg:justify-start mb-10">
                {{-- @if (hasFeature(\App\Models\TenantFeature::FEATURE_VALUATION_SYSTEM))
                    <a class="valuation-button whitespace-nowrap lg:text-sm text-base text-center tracking-wide font-bold rounded-3xl max-w-xs block py-3 px-8 transition-all cta"
                    href="{{ localeUrl('/valuation') }}">{{ trans('button.book_a_valuation') }}</a>
                @endif
            
                <a class="whitespace-nowrap lg:text-sm text-base text-center tracking-wide font-bold rounded-3xl max-w-xs block ml-2 sm:ml-4 py-3 px-8 transition-all cta" href="{{ localeUrl('/all-properties-for-sale') }}">
                    {{ trans('button.find_a_property') }}
                </a> --}}

                @if (hasFeature(\App\Models\TenantFeature::FEATURE_VALUATION_SYSTEM))
                    <a class="valuation-button whitespace-nowrap lg:text-sm text-base text-center font-bold rounded-3xl max-w-xs block py-3 px-8 transition-all mb-4 lg:mb-0 w-2/3 primary-bg text-white"
                        href="{{ localeUrl('/valuation') }}">{{ trans('button.book_a_valuation') }}</a>
                @endif

                <a style="border-color: var(--ap-cta-bg);" class="find-a-property-button whitespace-nowrap lg:text-sm text-base text-center font-bold rounded-3xl max-w-xs block lg:ml-4 py-3 px-8 transition-all w-2/3 border text-white" href="{{ localeUrl('/all-properties-for-sale') }}">
                    {{ trans('button.find_a_property') }}
                </a>
            </div>
            
            {{-- @include(themeViewPath('frontend.components.hero-reviews'), ['absolutelyPosition' => false]) --}}
        </div>

        <div class="w-full h-full overflow-hidden relative">
            <div class="absolute video-overlay w-full h-full inset bg-black z-10" @if($video->overlay_opacity) style="opacity: {{ $video->overlay_opacity }}" @else style="opacity: 0;" @endif></div>

            <div style="background-repeat: no-repeat; background-image: url('{{ assetPath($video->fallback_image) }}')" class="center-cover-bg h-full w-full"></div>
        </div>
    </div>
</div>
