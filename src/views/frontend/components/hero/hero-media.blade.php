<div class="secondary-bg hero-wrapper">
    <div class="relative">
        <div class="min-h-screen">
            @if ($video)

                @if ($video->fallback_image_enabled)
                    <!-- show video with image fallback -->
                    <div class="hidden hero-fallback-image-container h-screen">
                        @include(themeViewPath('frontend.components.hero.hero-fallback-image'))
                    </div>

                    <div class="hidden hero-video-container h-full">
                        @include(themeViewPath('frontend.components.hero.hero-video'))
                    </div>
                @else

                    <!-- show video without image fallback -->
                    @include(themeViewPath('frontend.components.hero.hero-video'))
                @endif


            @else

                <!-- show hero slideshow system -->
                @include(themeViewPath('frontend.components.hero.hero-slideshow'))
            @endif

        </div>
    </div>
</div>
