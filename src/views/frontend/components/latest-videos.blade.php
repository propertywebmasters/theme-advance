@if ($videos->count() > 0)
    @php
        $i = 0
    @endphp
    <section id="latest-videos">
        <div class="container py-12 px-4 md:px-0 mx-auto">
            <div class="items-baseline pb-6 text-center lg:text-left">
                <h2 class="header-text text-xl text-center md:text-left md:text-3xl  leading-tight tracking-tight text-primary">{{ trans('header.latest_videos') }}</h2>
            </div>
            <div class="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-8">
                @foreach($videos as $video)
                    @php $i++ @endphp
                    @include(themeViewPath('frontend.components.cards.video'), ['video' => $video, 'css' => $i == 4 ? 'lg:hidden xl:hidden' : ''])
                @endforeach
            </div>

            <div class="text-center pt-10">
                <a class="rounded-3xl inline-block primary-bg text-white md:text-lg py-2 px-4 hover-lighten" href="{{ localeUrl('/videos') }}">
                    {{  trans('header.view_archive') }} <img class="svg-inject inline-block text-white stroke-current fill-current h-4 pl-2" src="{{ themeImage('icons/right-arrow.svg') }}" loading="lazy">
                </a>
            </div>
        </div>
    </section>
    @php $i++ @endphp
@endif
