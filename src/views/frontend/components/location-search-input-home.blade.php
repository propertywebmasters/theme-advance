@php
    if (!isset($value)) {
        $value = '';
    }

    if (!isset($datalist)) {
        $datalist = false;
    }

    if (!isset($additionalClasses)) {
        $additionalClasses = '';
    }

    if (!isset($searchResultsContainer)) {
        $searchResultsContainer = 'search-results-container'; 
    }

    if (!isset($placeholder)) {
        $placeholder = trans('placeholder.search_location');
    }
@endphp

<div class="relative">
    @if ($datalist)
        <input class="pr-8 placeholder-black location-datalist-input {{ $additionalClasses }}"  autocomplete="off" role="combobox" list="" id="location_datalist" name="location" placeholder="{{ trans('placeholder.search_location') }}" value="{{ $value }}" style="text-overflow: ellipsis;">
    @else
        <div class="flex items-center">
            <div>
                <img style="color: var(--ap-cta-bg);" class="hidden lg:block svg-inject" src="{{ themeImage('search.svg') }}" alt="Search">
            </div>

            <div class="ml-2" style="flex-grow: 1;">
                <input id="search_location" name="location" type="text"
                class="pr-8 placeholder-black autocomplete-location w-full focus:outline-none bg-white {{ $additionalClasses }}"
                placeholder="{{ $placeholder }}" autocomplete="off"
                data-search-results-container="{{ $searchResultsContainer }}" style="text-overflow: ellipsis;" value="{{ $value }}">
            </div>
        </div>
    @endif

    <div class="clear-search-button absolute top-1/2 right-2 transform -translate-y-1/2 cursor-pointer {{ $value ? '' : 'hidden' }}">
        <i class="w-4 h-4" data-feather="x"></i>
    </div>
</div>
