@php
    $i = 0;
    $saleProperties = $properties->where('tender_type', 'sale');
    $rentalProperties = $properties->where('tender_type', 'rental');

    $isMultiMode = $saleProperties->count() > 0 && $rentalProperties->count() > 0;
@endphp

@if ($properties->count() > 0)
    @php
        $header = trans('header.similar_properties');
        if (isset($customHeader)) {
            $header = $customHeader;
        }
    @endphp

    <section id="featured-developments" class="mb-5 lg:mb-24">
        <div class="container mx-auto bg-white px-4 md:px-0">
            <div class="flex justify-between pb-8 pt-12 items-center">
                <div class="mx-auto text-center">
                    <h3 class="header-text text-xl text-center md:text-3xl leading-tight tracking-tight text-primary">{{ $customHeader }}</h3>
                </div>
                @if($isMultiMode)
                    <div class="flex">
                        <a href="javascript:" class="tab-switcher text-base text-right primary-text" data-on-classes="primary-text" data-show="tab-featured-sale" data-hide="tab-featured-rental">
                            {{ \Illuminate\Support\Str::plural(trans('label.sale')) }}
                        </a>
                        <span class="inline-block px-2">|</span>
                        <a href="javascript:" class="tab-switcher text-base" data-on-classes="primary-text" data-show="tab-featured-rental" data-hide="tab-featured-sale">
                            {{ \Illuminate\Support\Str::plural(trans('label.rental')) }}
                        </a>
                    </div>
                @endif
            </div>

            @if($isMultiMode)
                <div class="tab-featured-sale grid md:grid-cols-2 gap-4">
                    @php $i = 1 @endphp
                    @foreach($saleProperties as $property)
                        @php $extraCss = $i > 3 ? 'xl:hidden' : '' @endphp
                        @include(themeViewPath('frontend.components.cards.property'))
                        @php $i++ @endphp
                    @endforeach
                </div>

                <div class="tab-featured-rental hidden grid md:grid-cols-2 gap-4 hidden">
                    @php $i = 1 @endphp
                    @foreach($rentalProperties as $property)
                        @php $extraCss = $i > 3 ? 'xl:hidden' : '' @endphp
                        @include(themeViewPath('frontend.components.cards.property'))
                        @php $i++ @endphp
                    @endforeach
                </div>
            @else
                <div class="grid md:grid-cols-2 gap-4">
                    @php $i = 1 @endphp
                    @foreach($properties as $property)
                        @if($i > 9) @continue @endif
                        @include(themeViewPath('frontend.components.cards.property'))
                        @php $i++ @endphp
                    @endforeach
                </div>
            @endif

        </div>
    </section>


@endif
