@php use App\Models\Property; @endphp
@php use Illuminate\Support\Str; @endphp
@php use App\Models\TenantFeature; @endphp
@extends('layouts.app')

@push('open-graph-tags')
    @include(themeViewPath('frontend.components.property.open-graph'))
@endpush

@section('content')

    @php
        $hasImages = $property->images->first() !== null;

        $imageString = $captionString = '';
        foreach ($property->images as $image) {
            $imageString .= getPropertyImage($image->filepath, 'xl').'|';
            $captionString .= $image->caption.'|';
        }

        $imageString = trim($imageString, '|');
        $captionString = trim($captionString, '|');
        $imageCount = $property->images->count()
    @endphp

    {{-- site header component, includes navigation --}}
    @include(themeViewPath('frontend.components.header'))

    <section id="property-detail" class="mt-4 md:pt-2 lg:pt-0">
        <div class="container px-4 md:px-0 mx-auto">

            <div class="property-breadcrumb text-sm py-4 hidden lg:block">
                @include(themeViewPath('frontend.components.property-breadcrumb'))
            </div>

            <div class="flex flex-wrap">

                <div class="w-full lg:w-4/6 pr-0 lg:pr-12">
                    <div class="relative">
                        <div class="swiper custom-swiper-gallery-main overflow-hidden md:p-0 z-1 list-none relative">

                            @php
                                $mainClasses = 'h-54 max-h-54 md:h-114 md:max-h-114 md:h-114 md:max-h-114 lg:min-h-142 lg:h-142 lg:max-h-142 xl:h-142 xl:min-h-142 xl:max-h-142';
                                $thumbClasses = 'h-16 mx-auto overflow-hidden'
                            @endphp

                            <div class="swiper-wrapper flex z-1 relative w-full transition-transform box-content slider {{ $mainClasses }}" data-images="{{ $imageString }}"
                                 data-captions="{{ $captionString }}">

                                @if($property->tender_status !== Property::TENDER_STATUS_AVAILABLE)
                                    <div
                                        class="absolute tender-status top-2 right-2 primary-bg text-white p-2 rounded-l-2xl rounded-r-2xl z-10">{{ $property->tender_status }}</div>
                                @endif

                                @if ($hasImages)
                                    @foreach($property->images as $image)
                                        <div class="swiper-slide cursor-pointer start-gallery" data-img-src="{{ getPropertyImage($image->filepath, 'xl') }}" style="height: unset;">
                                            <img src="{{ getPropertyImage($image->filepath, 'xl') }}" alt="img" loading="lazy" class="w-full h-full object-cover">
                                        </div>
                                    @endforeach
                                @else
                                    <div class="swiper-slide">
                                        <img id="gallery-image-focus" src="/img/image-coming-soon.jpg" alt="img" loading="lazy" class="w-full cursor-pointer">
                                    </div>
                                @endif
                            </div>

                            <div class="sl-btns absolute bottom-2 left-2 md:bottom-7 md:left-4 xl:bottom-4 xl:left-4 flex z-30">
                                @if($useMap && $property->location->latitude !== null && $property->location->longitude !== null)
                                    <a href="javascript:"
                                       class="tertiary-bg text-white mr-2 leading-loose text-center font-bold text-xs md:text-base bg-opacity-70 px-4 py-1 rounded-full modal-button"
                                       data-target="property-location-modal">{{ trans('header.location') }}</a>
                                @endif
                                @if (!hasFeature(TenantFeature::FEATURE_DOCUMENTS_BY_TYPE))
                                    @if ($property->documents->count() > 0)
                                        <a href="javascript:"
                                           class="tertiary-bg text-white mr-2 leading-loose text-center font-bold text-xs md:text-base bg-opacity-70 px-4 py-1 rounded-full modal-button"
                                           data-target="property-documents-modal">{{ trans('header.documents') }}</a>
                                    @endif
                                @else
                                    @include(themeViewPath('frontend.components.property.document-by-type-buttons'))
                                @endif
                                @if ($property->video_url)
                                    <a href="javascript:"
                                       class="tertiary-bg text-white mr-2 leading-loose text-center font-bold text-white text-xs md:text-base bg-opacity-70 px-4 py-1 rounded-full modal-button"
                                       data-target="property-video-modal">{{ trans('header.video') }}</a>
                                @endif
                                @if ($property->virtual_tour_url)
                                    <a href="javascript:"
                                       class="tertiary-bg text-white mr-2 leading-loose text-center font-bold text-white text-xs md:text-base bg-opacity-70 px-4 py-1 rounded-full modal-button"
                                       data-target="property-virtual-tour-modal">{{ trans('header.virtual_tour') }}</a>
                                @endif
                            </div>
                            <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
                        </div>

                        <div class="absolute inset-y-1/2 w-full -translate-y-1/2 flex justify-between lg:block z-40">
                            <div class="swiper-button-prevBtn relative md:absolute md:left-4 top-0 cursor-pointer inline-block pr-2 md:pr-0">
                                <img src="{{ themeImage('icons/caret-right.svg') }}" class="svg-inject text-white fill-current stroke-current h-8" alt="arrow" loading="lazy"
                                     style="transform: rotate(180deg);">
                            </div>
                            <div class="swiper-button-nextBtn relative md:absolute md:right-4 top-0 cursor-pointer inline-block">
                                <img src="{{ themeImage('icons/caret-right.svg') }}" class="svg-inject text-white fill-current stroke-current h-8" alt="arrow" loading="lazy">
                            </div>
                        </div>
                    </div>

                    @if($hasImages)
                        <div class="swiper custom-swiper-gallery-thumbnails overflow-hidden md:p-0 z-1 list-none mt-2">
                            <div class="swiper-wrapper flex z-1 relative w-full transition-transform box-content">
                                @foreach($property->images as $i => $image)
                                    <div class="swiper-slide flex-shrink-0 cursor-pointer">
                                        <img class="custom-gallery-thumb {{ $thumbClasses }}" src="{{ getPropertyImage($image->filepath, 'sm') }}"
                                             data-original="{{ getPropertyImage($image->filepath, 'xl') }}" alt="img" loading="lazy" data-slide-number="{{ $i }}">
                                    </div>
                                @endforeach
                            </div>
                            <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
                        </div>
                    @endif
                </div>

                <div class="w-full lg:w-2/6 pb-10 py-0">
                    <div class="flex justify-between pl-1">
                        @if(stripos(url()->previous(), tenantManager()->getTenant()->domain) !== false || stripos(url()->previous(), tenantManager()->getTenant()->dev_domain) !== false)
                            <div class="flex">
                                <a href="{{ url()->previous() }}" class="leading-normal inline-block -mt-3">
                                    <img src="{{ themeImage('icons/angle-right-black.png') }}" alt="" class="inline-block transform rotate-180" loading="lazy">
                                    <span class="inline-block text-sm  leading-normal font-medium pl-1 py-3">{{ trans('search.back_to_search') }}</span>
                                </a>
                            </div>
                        @endif

                        <span class="tracking-tight text-base">{{ trans('label.reference') }}: {{ $property->displayReference() }}</span>
                    </div>
                    <div class="xl:mt-20 lg:mt-12 mt-5">

                        <div class="pl-1">

                            @include(themeViewPath('frontend.components.system-notifications'))

                            <h3 class="text-2xl leading-loose font-bold primary-text uppercase ">{!! $property->displayPrice() !!}</h3>

                            <div class="flex flex-wrap items-center justify-start">
                                @if(hasFeature(TenantFeature::FEATURE_ARRANGE_VIEWING_SYSTEM))
                                    <div class="text-center md:text-left mr-2 mb-2">
                                        <a href="javascript:" data-target="book-viewing-modal" class="leading-tight modal-button inline-block text-xs border rounded-3xl py-2 px-4"
                                           style="color: #575757; border-color: #575757;">{{ trans('header.book_viewing')}}</a>
                                    </div>
                                @endif
                                @if(hasFeature(TenantFeature::FEATURE_MORTGAGE_CALCULATOR) && $property->tender_type === Property::TENDER_TYPE_SALE)
                                    <div class="text-center md:text-left mr-2 mb-2">
                                        <a href="javascript:" class="leading-tight modal-button inline-block text-xs border rounded-3xl py-2 px-4"
                                           style="color: #575757; border-color: #575757;"
                                           data-target="mortgage-calculator-modal">{{ trans('header.mortgage_calculator') }}</a>
                                    </div>
                                @endif
                                @if(hasFeature(TenantFeature::FEATURE_STAMP_DUTY_CALCULATOR) && $property->tender_type === Property::TENDER_TYPE_SALE)
                                    <div class="text-center md:text-left mb-2">
                                        <a href="javascript:" class="leading-tight modal-button inline-block text-xs border rounded-3xl py-2 px-4"
                                           style="color: #575757; border-color: #575757;"
                                           data-target="stamp-duty-calculator-modal">{{ trans('header.stamp_duty') }}</a>
                                    </div>
                                @endif
                            </div>

                            <h1 class="header-text mt-3 block text-2xl xl:text-3xl leading-tight tracking-tight text-primary">{{ $property->displayName() }}</h1>
                            <p class="text-lg tracking-tight mt-3">{{ $property->location->displayAddress() }}</p>

                            <div class="mt-4">
                                @include(themeViewPath('frontend.components.property.bed-bath-info'), ['showTextSmallerScreens' => true, 'showSizing' => true,])
                            </div>

                            @php $buttonClasses = 'block align-middle w-full text-sm xl:text-base text-center tracking-wide font-bold rounded-full transition-all py-4 primary-bg text-white' @endphp

                        </div>

                        <div class="mt-12">
                            <div class="grid grid-cols-2 gap-4 mt-6">
                                <div class="table-cell align-middle">
                                    <a href="javascript:" data-target="property-enquiry-form"
                                       class="{{ $buttonClasses }} cta cta-text scroll-to hidden lg:block hover-cta-bg">{{ trans('button.enquire_now') }}</a>
                                    <a href="javascript:" data-target="request-details-modal"
                                       class="modal-button {{ $buttonClasses }} cta cta-text lg:hidden hover-cta-bg">{{ trans('button.enquire_now') }}</a>

                                </div>

                                <div class="table-cell align-middle">
                                    @if(hasFeature(TenantFeature::FEATURE_SHORTLIST_SYSTEM))
                                        @if(user())
                                            @php
                                                $shortlistText = in_array($property->uuid, $shortlistedProperties->pluck('property_uuid')->toArray())
                                                    ? trans('shortlist.saved')
                                                    : trans('shortlist.save')
                                            @endphp
                                            <a href="{{ localeUrl('/property/'.$property->url_key.'/toggle-shortlist') }}" class="{{ $buttonClasses }}">{{ $shortlistText }}</a>
                                        @else
                                            <a href="javascript:" data-target="preauth-modal"
                                               class="modal-button hover-cta-bg {{ $buttonClasses }}">{{ Str::limit(trans('shortlist.save'), 14) }}</a>
                                        @endif
                                    @endif
                                </div>

                                @if(user())
                                    <div class="table-cell align-middle">
                                        <a href="javascript:"
                                           data-target="property-note-modal"
                                           class="modal-button {{ $buttonClasses }} cta cta-text scroll-to">
                                            {{ trans('button.add_property_note') }}
                                        </a>
                                    </div>
                                @endif
                            </div>

                            <div class="mt-8 pt-4">
                                <p class="text-xl leading-loose text-center tracking-tight text-primary mb-2">{{ trans('generic.share_this_property') }}</p>
                                @include(themeViewPath('frontend.components.social.share'))
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </section>

    <section id="enquire_property" class="pt-3 mb-5 lg:mb-20">
        <div class="bg-whiter">
            <div class="container px-4 md:px-0 mx-auto pt-8 pb-14">
                <div class="flex flex-wrap">
                    <div class="w-full lg:w-4/6 md:pr-8 lg:pr-8 xl:pr-16">
                        @include(themeViewPath('frontend.components.property.description'))
                        @include(themeViewPath('frontend.components.property.features'))
                    </div>
                    <div class="w-full hidden lg:block lg:w-2/6">
                        <div id="property-enquiry-form" class="shadow-md border border-gray-100 py-8 px-4 lg:px-7 mt-4 bg-white">
                            <h3 class="header-text text-2xl leading-normal text-center tracking-tight text-primary mb-7">{{ trans('header.enquire_about_this_property') }}</h3>
                            <form action="{{ localeUrl('/property/'.$property->url_key.'/enquiry') }}" method="post" enctype="application/x-www-form-urlencoded" class="recaptcha">
                                <input type="text" name="name" placeholder="{{ trans('contact.full_name') }} *"
                                       class="rounded-full bg-whiter h-14 w-full px-4 mb-3 focus:outline-none" required>
                                <input type="email" name="email" placeholder="{{ trans('contact.email_address') }} *"
                                       class="rounded-full bg-whiter h-14 w-full px-4 mb-3 focus:outline-none" required>
                                <input type="text" name="tel" placeholder="{{ trans('contact.telephone_number') }}"
                                       class="rounded-full bg-whiter h-14 w-full px-4 mb-3 focus:outline-none">
                                <textarea name="comment" cols="30" rows="10" placeholder="{{ trans('contact.your_message') }} *"
                                          class="rounded-3xl bg-whiter h-32 w-full px-4 py-3 mb-3 focus:outline-none" required></textarea>

                                @include(themeViewPath('frontend.forms.recaptcha.recaptcha-fallback-inputs'), ['inputClass' => 'rounded-full bg-whiter h-14 w-full px-4 mb-3 focus:outline-none'])

                                <div class="text-center">
                                    <button type="submit"
                                            class="cta text-base text-center tracking-wide font-bold px-4 sm:px-9 rounded-full h-9 sm:h-12 right-1 top-1 cta-bg cta-text transition-all">{{ trans('button.send_enquiry') }}</button>
                                </div>
                                @csrf
                            </form>

                            @if (shouldShowWhatsappCta())
                                <div class="my-2 text-center">
                                    <span class="uppercase">{{ trans('generic.or') }}</span>
                                </div>

                                <div class="text-center">
                                    @include(themeViewPath('frontend.components.whatsapp-cta'), compact('property'))
                                </div>
                            @endif
                        </div>

                        @include(themeViewPath('frontend.components.contact-details'))
                    </div>
                </div>

            </div>
        </div>
    </section>

    @include(themeViewPath('frontend.components.featured-properties'), ['customHeader' => trans('header.similar_properties'), 'properties' => $relatedProperties])

    {{-- site footer --}}
    @include(themeViewPath('frontend.components.footer'))

    {{-- CTA Modals --}}

    @if ($property->video_url !== null)
        @include(themeViewPath('frontend.components.modals.video'))
    @endif
    @if ($property->virtual_tour_url !== null)
        @include(themeViewPath('frontend.components.modals.virtual-tour'))
    @endif
    @if($useMap && $property->location->latitude !== null && $property->location->longitude !== null)
        @include(themeViewPath('frontend.components.modals.location'))
    @endif
    @include(themeViewPath('frontend.components.modals.gallery-overlay'))
    @include(themeViewPath('frontend.components.modals.gallery'))
    @include(themeViewPath('frontend.components.modals.request-details'))
    @include(themeViewPath('frontend.components.modals.book-viewing'))
    @include(themeViewPath('frontend.components.modals.property-note'))
    @include(themeViewPath('frontend.components.modals.mortgage-calculator'))
    @include(themeViewPath('frontend.components.modals.stamp-duty-calculator'))
    @include(themeViewPath('frontend.components.modals.property-note'))
    @include(themeViewPath('frontend.components.modals.share-this-property'))

    @if (hasFeature(TenantFeature::FEATURE_PDF_BEHIND_FORM))
        @include(themeViewPath('frontend.components.modals.brochure-request'))
    @endif

    @if (!hasFeature(TenantFeature::FEATURE_DOCUMENTS_BY_TYPE))
        @if ($property->documents->count() > 0)
            @include(themeViewPath('frontend.components.modals.documents'))
        @endif
    @else
        @if ($floorplanDocs->count())
            @include(themeViewPath('frontend.components.modals.documents-by-type'), ['id' => 'property-floorplan-documents-modal', 'documents' => $floorplanDocs])
        @endif

        @if ($epcDocs->count())
            @include(themeViewPath('frontend.components.modals.documents-by-type'), ['id' => 'property-epc-documents-modal', 'documents' => $epcDocs])
        @endif

        @if ($brochureDocs->count())
            @include(themeViewPath('frontend.components.modals.documents-by-type'), ['id' => 'property-brochure-documents-modal', 'documents' => $brochureDocs])
        @endif

        @if ($otherDocs->count())
            @include(themeViewPath('frontend.components.modals.documents-by-type'), ['id' => 'property-other-documents-modal', 'documents' => $otherDocs])
        @endif
    @endif
@endsection
