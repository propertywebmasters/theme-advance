@extends('layouts.app')

@section('content')

    {{-- site header component, includes navigation --}}
    @include(themeViewPath('frontend.components.header'))

    @php
        use App\Models\TenantFeature;
        $multiColumn = hasFeature(TenantFeature::FEATURE_GENERIC_CONTACT_FORM);
        $breadcrumb['/'] = trans('header.home');

        if ($page->parent !== null) {
            $breadcrumb['/'.$page->parent->url_key] = $page->parent->title;
        }

        $breadcrumb['#'] = $page->title;

        $searchControls = searchControlOptions(config('platform-cache.search_controls_enabled'))
    @endphp

    <section id="generic-page" class="bg-whiter py-12 lg:py-16">
        <div class="container px-4 mx-auto">
            @include(themeViewPath('frontend.components.system-notifications'), ['customClass' => 'mb-6'])

            <div class="flex flex-col lg:flex-row lg:items-center lg:justify-between">
                <div class="mb-4 lg:mb-0">
                    <h1 class="text-2xl lg:text-3xl xl:text-5xl tracking-tight font-bold text-primary pb-2 lg:pb-8 header-text">{!! $page->title !!}</h1>

                    <!--  Breadcrumb -->
                    <div class="generic-breadcrumb text-xs lg:text-base">
                        @php $i = 1 @endphp
                        @foreach($breadcrumb as $url => $anchor)
                            <a class="cta-bg-text-only text-xs lg:text-base" href="{{ $url }}">{!! $anchor !!}</a>
                            @if ($i < count($breadcrumb)) &gt; @endif
                            @php $i++ @endphp
                        @endforeach
                    </div>
                </div>

                @if (isset($searchControls->tender_type))
                    <div class="flex items-center lg:justify-end">
                        @if ($searchControls->tender_type === 'sale_only')
                            <a id="valuation-cta" class="text-sm md:text-base inline-block leading-loose text-center tracking-wide font-bold primary-bg text-white rounded-md lg:rounded-full md:py-3 py-2 px-8 lg:px-12 transition-all whitespace-nowrap" href="{{ localeUrl('/all-properties-for-sale') }}">
                                {{ trans('search.search_sales') }}
                            </a>
                        @elseif ($searchControls->tender_type === 'rental_only')
                            <a id="valuation-cta" class="text-sm md:text-base inline-block leading-loose text-center tracking-wide font-bold primary-bg text-white rounded-md lg:rounded-full md:py-3 py-2 px-8 lg:px-12 transition-all  whitespace-nowrap" href="{{ localeUrl('/all-properties-for-rent') }}">
                                {{ trans('search.search_rentals') }}
                            </a>
                        @else
                            <a id="valuation-cta" class="text-sm md:text-base inline-block leading-loose text-center tracking-wide font-bold primary-bg text-white rounded-md lg:rounded-full md:py-3 py-2 px-8 lg:px-12 transition-all  whitespace-nowrap mr-2" href="{{ localeUrl('/all-properties-for-rent') }}">
                                {{ trans('search.search_rentals') }}
                            </a>

                            <a id="valuation-cta" class="text-sm md:text-base inline-block leading-loose text-center tracking-wide font-bold secondary-bg text-white rounded-md lg:rounded-full md:py-3 py-2 px-8 lg:px-12 transition-all  whitespace-nowrap " href="{{ localeUrl('/all-properties-for-sale') }}">
                                {{ trans('search.search_sales') }}
                            </a>
                        @endif
                    </div>
                @endif
            </div>
        </div>
    </section>

    <section class="py-8">
        <div class="container px-4 mx-auto">
            <div class="flex flex-wrap">
                <div class="w-full lg:w-4/6 pr-0 md:pr-8">
                    <div class="mb-16">
                        <h3 class="text-2xl leading-loose tracking-tight font-bold text-primary">{{ $page->subtitle }}</h3>
                        @if ($page->image)
                            <img src="{{ assetPath($page->image) }}" class="w-full h-full object-cover object-center mb-6">
                        @endif

                        <div class="generic-page-content">{!! parseContentForShortcodes($page->content) !!}</div>
                    </div>
                </div>
                <div class="w-full lg:w-2/6">

                    <div class="bg-whiter">
                        @if($pageNavigation->count() > 0)
                            <div class="w-full primary-bg text-white text-base pr-2 mb-4">
                                <form>
                                    <select id="sort" name="sort" class="autojump w-full block focus:outline-none leading-normal text-base primary-bg text-white p-2">
                                        @foreach($pageNavigation as $navigation)
                                            @php $selected = trim($_SERVER['REQUEST_URI'], '/') === $navigation->url_key ? 'selected="selected"' : '' @endphp
                                            <option class="primary-bg text-white" value="{{ url($navigation->url_key) }}" {{ $selected }}>{{ $navigation->title }}</option>
                                        @endforeach
                                    </select>
                                </form>
                            </div>
                        @endif

                        <div class="py-9 px-4 lg:px-7 generic-contact-form">
                            <h3 class="text-2xl leading-normal text-center tracking-tight font-bold text-primary mb-7">{{ trans('contact.get_in_touch') }}</h3>
                            <form action="{{ url()->current() }}" method="post" enctype="application/x-www-form-urlencoded" class="recaptcha">
                                <input type="text" name="name" placeholder="{{ trans('contact.full_name') }}" class="rounded-full border h-14 w-full px-4 mb-3">
                                <input type="email" name="email" placeholder="{{ trans('contact.email_address') }}" class="rounded-full border h-14 w-full px-4 mb-3">
                                <input type="text" name="tel" placeholder="{{ trans('contact.telephone_number') }}" class="rounded-full border h-14 w-full px-4 mb-3">
                                <textarea name="message" id="" cols="30" rows="10" placeholder="{{ trans('contact.your_message') }}" class="rounded-3xl border h-32 w-full px-4 py-3 mb-3"></textarea>
                                <input type="hidden" name="url" value="{{ url()->current() }}">

                                @include(themeViewPath('frontend.forms.recaptcha.recaptcha-fallback-inputs'), ['inputClass' => 'rounded-full border h-14 w-full px-4 mb-3'])

                                <div class="text-center">
                                    <button type="submit" class="text-base text-center tracking-wide font-bold text-white cta px-4 sm:px-9 rounded-full h-9 sm:h-12 right-1 top-1 hover:bg-secondary transition-all">{{ trans('button.send_enquiry') }}</button>
                                </div>
                                @csrf
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

    {{-- site footer --}}
    @include(themeViewPath('frontend.components.footer'))

@endsection
