@extends('layouts.app')

@section('content')

    @php
        $searchControls = searchControlOptions(config('platform-cache.search_controls_enabled'))
    @endphp

    {{-- site header component, includes navigation --}}
    @include(themeViewPath('frontend.components.header'))

    <section class="bg-whiter pb-12 xl:pb-24">
        <div class="container mx-auto px-4 xl:pt-16">
            <h2 class="header-text py-9 pb-6 lg:pb-10 xl:pb-20 text-2xl lg:text-3xl xl:text-5xl text-center tracking-tight text-primary max-w-xl mx-auto">{{ $areaGuide->main_title }}</h2>
            <div class=" relative flex items-center justify-center">
                <div class="text-center">
                    <img class="rounded-2xl h-80 sm:h-auto object-cover" src="{{ assetPath($areaGuide->image) }}" alt="1">
                </div>

                <form id="home-search" action="{{localeUrl('/search')}}" method="post" enctype="application/x-www-form-urlencoded" class="w-full absolute max-w-xl">
                    <div class="flex w-full flex-col sm:flex-row">

                        @if(isset($searchControls->tender_type) && $searchControls->tender_type === 'both')
                            <div class="w-auto md:min-w-12 lg:min-w-24">
                                <select name="type" id="search_type" class="sm:rounded-r-none rounded-full h-14 px-10 text-browngrey appearance-none select-carat hidden sm:block" style="background-image: url('{{ themeImage('arrow-back.svg') }}'); background-repeat: no-repeat; background-position: 97% 25px;">
                                    <option value="sale">{{ trans('label.sale') }}</option>
                                    <option value="rental">{{ trans('label.rental') }}</option>
                                </select>
                            </div>
                        @elseif(isset($searchControls->tender_type) && ($searchControls->tender_type === 'sale_only' || $searchControls->tender_type === 'rental_only'))
                            @if($searchControls->tender_type === 'sale_only')
                                <input id="search_type" name="type" type="hidden" value="sale">
                            @elseif($searchControls->tender_type === 'rental_only')
                                <input id="search_type" name="type" type="hidden" value="rental">
                            @endif
                        @endif

                        <div class="relative w-3/4 mx-auto sm:w-full mt-2 sm:mt-0">
                            <div class="absolute top-2 sm:h-10 h-7 w-0.5 bg-gray-300 left-2 opacity-0 sm:opacity-100 z-10">
                            </div>
                            <div class="bg-white w-full rounded-full h-11 sm:h-14">
                                <div class="flex items-center h-full">
                                    <div style="flex: 1;">
                                        @include(themeViewPath('frontend.components.location-search'), [
                                            'additionalClasses' => 'sm:rounded-l-none rounded-full h-11 sm:h-14 sm:px-10 focus:border-0 md:text-base text-sm px-3',
                                            'value' => $areaGuide->location,
                                            'placeholder' => '',
                                            'areaGuide' => true
                                        ])
                                    </div>

                                    <button type="submit"
                                            class="text-base text-center tracking-wide font-bold text-white uppercase cta px-4 sm:px-9 rounded-full h-9 sm:h-12 mr-1 hover-primary-bg transition-all">
                                        {{ trans('label.search') }}
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </section>

    <section class="bg-white pt-6 lg:pt-16">
        <div class="container mx-auto px-4 pb-16">
            <div class="max-w-5xl mx-auto">
                <h3 class="header-text text-2xl leading-loose tracking-tight text-primary mb-6">{{ $areaGuide->main_title }}</h3>
                <p class="text-base leading-normal tracking-tight">{!! $areaGuide->main_content !!}</p>
            </div>
            <div class="grid grid-cols-1 md:grid-cols-2 gap-4 pt-16 pb-8">
                @if ($areaGuide->images->count() > 0)
                    @foreach($areaGuide->images->pluck('filepath')->toArray() as $img)
                        <div>
                            <img class="w-full" src="{{ assetPath($img) }}" alt="img">
                        </div>
                    @endforeach
                @endif
            </div>

        </div>
    </section>

{{--    @include(themeViewPath('frontend.components.featured-properties'), ['customHeader' => trans('header.similar_properties'), 'properties' => $relatedProperties])--}}


    @include(themeViewPath('frontend.components.latest-news'), ['customHeader' => $newsIsRelated ? trans('header.latest_news_in').' '.$areaGuide->location : trans('header.latest_news')])

    {{-- site footer --}}
    @include(themeViewPath('frontend.components.footer'))

@endsection
