@php use App\Services\SiteSetting\Contracts\SiteSettingServiceInterface; @endphp
@php use App\Models\SiteSetting; @endphp
@extends('layouts.app')

@section('content')

    @php
        $siteEmail = app()->make(SiteSettingServiceInterface::class)->retrieve(SiteSetting::SETTING_SITE_EMAIL['key']);
        $siteTel = app()->make(SiteSettingServiceInterface::class)->retrieve(SiteSetting::SETTING_SITE_TEL['key']);
        $whatsappTel = app()->make(SiteSettingServiceInterface::class)->retrieve(SiteSetting::SETTING_SITE_WHATSAPP_NUMBER['key']);
    @endphp

    {{-- site header component, includes navigation --}}
    @include(themeViewPath('frontend.components.header'))

    <section class="bg-whiter pb-48">
        <div class="container mx-auto px-4 center-cover-bg bg-lazy-load">
            <h2 class="py-9 text-3xl lg:text-4xl xl:text-5xl header-text text-center tracking-tight text-primary max-w-xl mx-auto pt-20">{!! dynamicContent($pageContents, 'contact-title') !!}</h2>
            <p class="pb-10 text-xl text-center tracking-tight text-browngrey flex flex-col sm:flex-row justify-center contact-quick-info items-center">
                <a href="mailto:{{ $siteEmail }}" class="flex">
                    <img src="{{ themeImage('email2.svg') }}" class="svg-inject text-cta fill-current h-4 mr-2 mt-2" alt="marker" loading="lazy"> {{ $siteEmail }}
                </a>
                <a href="tel:{{ $siteTel }}" class="flex sm:ml-6">
                    <img src="{{ themeImage('phone.svg') }}" alt="img" class="svg-inject text-cta fill-current h-4 mr-2 mt-2" loading="lazy"> {{ $siteTel }}
                </a>

                @if($whatsappTel !== null && $whatsappTel !== '')
                    <a href="https://api.whatsapp.com/send?phone={{ $whatsappTel }}" class="flex sm:ml-6">
                        <img class="svg-inject fill-current stroke-current text-cta h-6 ml-2 mr-2" src="{{ themeImage('whatsapp-alt.svg') }}" alt="whatsapp"
                             loading="lazy"> {{ $whatsappTel }}
                    </a>
                @endif

            </p>

            @include(themeViewPath('frontend.components.system-notifications'), ['customClass' => 'mb-6'])

        </div>

    </section>

    <section class="mb-16">
        <div class="container mx-auto px-4">
            <div class="shadow-lg max-w-4xl mx-auto py-9 px-4 lg:px-11 -mt-40 bg-white rounded-2xl ">
                @include(themeViewPath('frontend.components.forms.contact-form'))
            </div>
        </div>
    </section>

    @include(themeViewPath('frontend.components.contact-branches'))

    {{-- site footer --}}
    @include(themeViewPath('frontend.components.footer'))

@endsection
