@extends('layouts.app')

@section('content')

    @push('open-graph-tags')
        @include(themeViewPath('frontend.components.home-open-graph'))
    @endpush

    {{-- site header component, includes navigation --}}
    @include(themeViewPath('frontend.components.header'))
    
    {{-- Primary hero element --}}
    @include(themeViewPath('frontend.components.hero.hero-media'))
    

    <div style="background: #F8F8F8;">
        <div class="container mx-auto border-b px-4 md:px-0">
            <div class="py-20 xl:px-80">
                <h1 class="text-center text-2xl xl:text-3xl mb-5 header-text">{{ trans('header.find_your_next_home') }}</h1>
                @include(themeViewPath('frontend.components.home-search'))
            </div>
        </div>

        {{-- Popular searches band --}}
        @include(themeViewPath('frontend.components.popular-searches'))
    </div>
    

    {{-- Featured properties band --}}
    <div class="bg-white">
        @include(themeViewPath('frontend.components.featured-properties'), ['customHeader' => trans('header.featured_properties'), 'properties' => $featuredProperties])
    </div>

    {{-- About us band --}}
    @include(themeViewPath('frontend.components.about'))

    {{-- About us band --}}
{{--    @include(themeViewPath('frontend.components.features'))--}}

    {{-- Testimonials band --}}
    @include(themeViewPath('frontend.components.testimonials'))

    {{-- Testimonials band --}}
    @include(themeViewPath('frontend.components.latest-videos'))

    {{-- Featured developments band --}}
{{--    @include(themeViewPath('frontend.components.featured-developments'))--}}

    {{-- Latest news band --}}
    @include(themeViewPath('frontend.components.latest-news'))

    {{-- site footer --}}
    @include(themeViewPath('frontend.components.footer'))

@endsection
