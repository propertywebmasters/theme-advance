document.addEventListener("DOMContentLoaded", function (event) {
    const mobileMenu = document.querySelector(".mobile-menu"),
        header = document.querySelector('#main-nav > div:first-of-type > div:first-of-type');

    document.querySelectorAll(".tham").forEach(el => {
        el.addEventListener("click", function () {
            if (mobileMenu.classList.contains('hidden')) {
                header.classList.add('navigation-bg');
            } else {
                header.classList.remove('navigation-bg');
            }

            mobileMenu.classList.toggle("hidden");
            document.querySelectorAll(".tham").forEach(el => el.classList.toggle('tham-active'));
        });
    });

    const homeSlides = document.querySelector('div#custom-home-slides');
    let interval = 0;

    if (homeSlides) {
        let slideInterval = homeSlides.getAttribute('data-speed');

        interval = window.setInterval(nextSlide, parseInt(slideInterval));
    }

    function nextSlide() {

        let currentSlide = getCurrentSlide();
        let nextSlide = getNextSlide(currentSlide);

        // set state of slides
        currentSlide.classList.add('hidden', 'opacity-0')
        currentSlide.classList.remove('active');
        nextSlide.classList.remove('hidden', 'opacity-0')
        nextSlide.classList.add('active');
        nextSlideTitle = nextSlide.getAttribute('data-title');
        nextSlideSubtitle = nextSlide.getAttribute('data-subtitle');
        
        document.querySelector('#slide-title').textContent = nextSlideTitle;

        if (nextSlideSubtitle) {
            document.querySelector('#slide-subtitle').textContent = nextSlideSubtitle;
            document.querySelector('#slide-subtitle').classList.remove('hidden');
        } else {
            document.querySelector('#slide-subtitle').classList.add('hidden');
        }
    }

    function getCurrentSlide() {
        return document.querySelector('div#custom-home-slides div.active');
    }

    function getNextSlide(currentSlide) {

        let currentNumber = parseInt(currentSlide.getAttribute('data-slide-number'));
        currentNumber++;
        
        let nextSlide = document.querySelector('div#custom-home-slides div.slide-' + currentNumber);
        if (nextSlide) {
            return nextSlide;
        }
        
        return document.querySelector('div#custom-home-slides div.slide-1');
    }

    const mainNav = document.querySelector('#main-nav'),
            body = document.getElementsByTagName('body')[0],
            stickyNav = document.querySelector('#sticky-nav');

    customScrollBody();

    window.onscroll = function (e) {
        customScrollBody();
    }

    function customScrollBody() {
        if (mainNav && stickyNav && !body.classList.contains('thankyou-index')) {
            const { pageYOffset } = window;
            const { scrollTop, scrollHeight } = mainNav;
            const { offsetHeight } = stickyNav;

            if (pageYOffset >= (scrollTop + scrollHeight)) {
                stickyNav.classList.remove('-top-full', 'opacity-0');
                stickyNav.classList.add('top-0');
                mobileMenu.classList.remove('absolute');
                mobileMenu.classList.add('fixed');
                mobileMenu.style.top = `${offsetHeight}px`;
            } else {
                stickyNav.classList.add('-top-full', 'opacity-0');
                stickyNav.classList.remove('top-0');
                mobileMenu.classList.remove('fixed');
                mobileMenu.classList.add('absolute');
                mobileMenu.style.removeProperty('top');
            }
        }
    }

    document.querySelectorAll('.custom-inline-gallery-control').forEach((el) => {
        el.classList.remove('hidden');
    });
    
    document.querySelectorAll('div.custom-inline-listing-gallery div.previous-image, div.custom-inline-listing-gallery div.next-image').forEach(function (el) {
        el.addEventListener('click', function () {
            const parent = this.closest('.custom-inline-listing-gallery');
            const imageString = parent.getAttribute('data-gallery_images');
            let images = imageString.split('|').map(str => str.replace(/['"]+/g, ''));

            if (images.length < 3) {
                for (let i = 0; i < Math.abs(images.length - 3); i++) {
                    images.push('/img/md/image-coming-soon.jpg');
                }
            }

            const action = this.classList.contains('next-image') ? 'next' : 'prev';
            const img1 = document.querySelector(`#${this.getAttribute('data-target')} .img-1`);
            const img2 = document.querySelector(`#${this.getAttribute('data-target')} .img-2`);
            const img3 = document.querySelector(`#${this.getAttribute('data-target')} .img-3`);

            let currentIndex = 0, 
                newIndex = 0, 
                currentImageSrc = img1.getAttribute('data-current-image-src');

            for (var i = 0; i < images.length; i++) {
                if (currentImageSrc === images[i]) {
                    currentIndex = i;
                }
            }

            if (action === 'next') {
                newIndex = currentIndex === 2 ? 0 : currentIndex + 1;
            } else {
                newIndex = currentIndex === 0 ? 2 : currentIndex - 1;
            }

            img1.style.backgroundImage = 'url("' + images[newIndex] + '")';
            img1.setAttribute('data-current-image-src', images[newIndex]);

            if (action === 'next') {
                newIndex = newIndex === 2 ? 0 : newIndex + 1;
            } else {
                newIndex = newIndex === 0 ? 2 : newIndex - 1;
            }

            img2.style.backgroundImage = 'url("' + images[newIndex] + '")';

            if (action === 'next') {
                newIndex = newIndex === 2 ? 0 : newIndex + 1;

            } else {
                newIndex = newIndex === 0 ? 2 : newIndex - 1;
            }

            img3.style.backgroundImage = 'url("' + images[newIndex] + '")';
        });
    });

    function positionMobileMenu() {
        const mobileMenu = document.querySelector('.mobile-menu'),
        nav = document.querySelector('#main-nav'),
        paddingTop = window.getComputedStyle(nav).getPropertyValue('padding-top');
        mobileMenu.style.top = `calc(${nav.offsetHeight}px - ${paddingTop})`;
    }

    const breakpoint = window.matchMedia('(min-width: 1024px)');

    const breakpointChecker = function() {
        if (breakpoint.matches === true) {
            const mobileMenu = document.querySelector('.mobile-menu');
            mobileMenu.style.top = 'unset';
        } else if (breakpoint.matches === false) {
           return positionMobileMenu();
        }
     };

     breakpoint.addEventListener('change', breakpointChecker);

     breakpointChecker();
});