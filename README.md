# Installation
- composer require propertywebmasters/advance-theme

# Publish Assets (Required)
- php artisan vendor:publish --tag=advance-theme-assets

# Publish Blade Files
- php artisan vendor:publish --tag=advance-theme-views
